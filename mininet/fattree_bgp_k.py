from ast import dump
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.cli import CLI
import logging
import os
import sys
import time
import networkx as nx
import json

logging.basicConfig(filename='/home/mininet/fat-tree-emulators/mininet/logs/fattree_bgp.log', level=logging.DEBUG)
logger = logging.getLogger(__name__)
home_dir = "/home/mininet"
with_captures = False

class FatTree( Topo ):
  
	CoreRouterList = []
	CoreRouterListASNs = []
	SpineRouterList = []
	SpineRouterListASNs = []
	LeafRouterList = []
	LeafRouterListASNs = []
	HostList = []

	def __init__( self, k):

		"Create Fat Tree topology"
		self.pod = k
		self.iCoreLayerRouter = int((k/2)**2)
		self.iSpineLayerRouter = int(k*k/2)
		self.iLeafLayerRouter = int(k*k/2)
		self.paths=dict()
		self.prefixes=dict()
		
		if hosts < 0:
			self.density = int(k/2)
		else:
			self.density = hosts
			
		self.iHost = int(self.iLeafLayerRouter * self.density)
		
		showMessage("hosts: " + str(hosts))
		showMessage("self.pod: " + str(self.pod))
		showMessage("self.iCoreLayerRouter: " + str(self.iCoreLayerRouter))
		showMessage("self.iSpineLayerRouter: " + str(self.iSpineLayerRouter))
		showMessage("self.iLeafLayerRouter: " + str(self.iLeafLayerRouter))
		showMessage("self.density: " + str(self.density))
		showMessage("self.iHost: " + str(self.iHost))
		
		self.bw_c2a = 0.2
		self.bw_a2e = 0.1
		self.bw_h2a = 0.05

		# Init Topo
		Topo.__init__(self)
  
		self.createTopo()
		showMessage("Finished topology creation!")

		self.createLink( bw_c2a=self.bw_c2a, 
						 bw_a2e=self.bw_a2e, 
						 bw_h2a=self.bw_h2a)
		showMessage("Finished adding links!")
		
	def createTopo(self):
		self.createCoreLayerRouter(self.iCoreLayerRouter)
		self.createSpineLayerRouter(self.iSpineLayerRouter)
		self.createLeafLayerRouter(self.iLeafLayerRouter)
		self.createHost(self.iHost)

	"""
	Create Router and Host
	"""

	def _addRouter(self, number, level, Router_list):
		for x in range(1, number+1):
			PREFIX = str(level) + "000"
			if x >= int(10):
				PREFIX = str(level) + "00"
			if x >= int(100):
				PREFIX = str(level) + "0"
			# Router_list.append(self.addHost('r' + PREFIX + str(x)))
			Router_list.append(self.addSwitch('r' + PREFIX + str(x), inNamespace=True))

	def createCoreLayerRouter(self, NUMBER):
		showMessage("Create Core Layer")
		self._addRouter(NUMBER, 1, self.CoreRouterList)

	def createSpineLayerRouter(self, NUMBER):
		showMessage("Create Spine Layer")
		self._addRouter(NUMBER, 2, self.SpineRouterList)

	def createLeafLayerRouter(self, NUMBER):
		showMessage("Create Leaf Layer")
		self._addRouter(NUMBER, 3, self.LeafRouterList)

	def createHost(self, NUMBER):
		showMessage("Create Host")
		for x in range(1, NUMBER+1):
			PREFIX = "h"
			self.HostList.append(self.addHost(PREFIX + str(x)))

	"""
	Add Link
	"""
	def createLink(self, bw_c2a=0.2, bw_a2e=0.1, bw_h2a=0.5):
		showMessage("Add link Core to Spine.")
		end = int(self.pod/2)
		for x in range(0, self.iSpineLayerRouter, end):
			for i in range(0, end):
				for j in range(0, end):
					linkopts = dict(bw=bw_c2a) 
					self.addLink(
						self.CoreRouterList[i*end+j],
						self.SpineRouterList[x+i],
						**linkopts)

		showMessage("Add link Spine to Leaf.")
		for x in range(0, self.iSpineLayerRouter, end):
			for i in range(0, end):
				for j in range(0, end):
					linkopts = dict(bw=bw_a2e) 
					self.addLink(
						self.SpineRouterList[x+i],
						self.LeafRouterList[x+j],
						**linkopts)

		showMessage("Add link Leaf to Host.")
		for x in range(0, self.iLeafLayerRouter):
			for i in range(0, self.density):
				linkopts = dict(bw=bw_h2a) 
				self.addLink(
					self.LeafRouterList[x],
					self.HostList[self.density * x + i],
					**linkopts)
	
def configRouterInterfaces(net,topo):
	end = int(topo.pod/2)
	link_configured = 0
	octet = 0
	for x in range(0, topo.iSpineLayerRouter, end):
		core_interface_index = int(x / end)
		for i in range(0, end):
			spine_router = net.getNodeByName(topo.SpineRouterList[x+i])
			spine_router.cmd("echo 1 > /proc/sys/net/ipv4/ip_forward")
			spine_zebra_file = open(f"{home_dir}/conf/zebra_" + spine_router.name + ".conf","a")
			spine_bgpd_file = open(f"{home_dir}/conf/bgpd_" + spine_router.name + ".conf","a")
			for j in range(0, end):
				if(link_configured>=254):
					octet += 1
					link_configured = 0
				spine_interface_index = j
				core_router = net.getNodeByName(topo.CoreRouterList[i*end+j])
				core_router.cmd("echo 1 > /proc/sys/net/ipv4/ip_forward")
				if_name_core = f"{core_router.name}-eth{core_interface_index+1}"
				if_name_spine = f"{spine_router.name}-eth{spine_interface_index+1}"
				core_router.cmd(f"ifconfig {if_name_core} 10.{octet}.{link_configured}.1/30")
				spine_router.cmd(f"ifconfig {if_name_spine} 10.{octet}.{link_configured}.2/30")
				if with_captures:
					core_router.cmd(f"tcpdump -i {if_name_core} -w {home_dir}/pcaps/{core_router.name}-{spine_router.name}-{if_name_core}.pcap  > /dev/null 2>&1 &")
					#spine_router.cmd(f"tcpdump -i {if_name_spine} -w {home_dir}/pcaps/{spine_router.name}-{core_router.name}-{if_name_spine}.pcap  > /dev/null 2>&1 &")
				
				core_bgpd_file = open(f"{home_dir}/conf/bgpd_" + core_router.name + ".conf","a")
				core_bgpd_file.write(f" neighbor {if_name_core} interface peer-group fabric\n")
				core_bgpd_file.close()

				core_zebra_file = open(f"{home_dir}/conf/zebra_" + core_router.name + ".conf","a")
				core_zebra_file.write(f"interface {if_name_core}\n")
				core_zebra_file.write(f" ip address 10.{octet}.{link_configured}.1/30\n")
				core_zebra_file.close()

				spine_zebra_file.write(f"interface {if_name_spine}\n")
				spine_zebra_file.write(f" ip address 10.{octet}.{link_configured}.2/30\n")

				spine_bgpd_file.write(f" neighbor {if_name_spine} interface peer-group fabric\n")

				link_configured = link_configured + 1
			spine_zebra_file.close()
			spine_bgpd_file.close()

	for x in range(0, topo.iSpineLayerRouter, end):
		for i in range(0, end):
			leaf_interface_index = i
			spine_router = net.getNodeByName(topo.SpineRouterList[x+i])
			spine_zebra_file = open(f"{home_dir}/conf/zebra_" + spine_router.name + ".conf","a")
			spine_bgpd_file = open(f"{home_dir}/conf/bgpd_" + spine_router.name + ".conf","a")
			for j in range(0, end):
				if(link_configured>=254):
					octet += 1
					link_configured = 0
				spine_interface_index = j + end
				leaf_router = net.getNodeByName(topo.LeafRouterList[x+j])
				if_name_leaf = f"{leaf_router.name}-eth{leaf_interface_index+1}"
				if_name_spine = f"{spine_router.name}-eth{spine_interface_index+1}"
				leaf_router.cmd(f"ifconfig {if_name_leaf} 10.{octet}.{link_configured}.2/30")
				spine_router.cmd(f"ifconfig {if_name_spine} 10.{octet}.{link_configured}.1/30")
				if with_captures:
					leaf_router.cmd(f"tcpdump -i {if_name_leaf} -w {home_dir}/pcaps/{leaf_router.name}-{spine_router.name}-{if_name_leaf}.pcap  > /dev/null 2>&1 &")
					#spine_router.cmd(f"tcpdump -i {if_name_spine} -w {home_dir}/pcaps/{spine_router.name}-{leaf_router.name}-{if_name_spine}.pcap  > /dev/null 2>&1 &")

				leaf_bgpd_file = open(f"{home_dir}/conf/bgpd_" + leaf_router.name + ".conf","a")
				leaf_bgpd_file.write(f" neighbor {if_name_leaf} interface peer-group TOR\n")
				leaf_bgpd_file.close()

				leaf_zebra_file = open(f"{home_dir}/conf/zebra_" + leaf_router.name + ".conf","a")
				leaf_zebra_file.write(f"interface {if_name_leaf}\n")
				leaf_zebra_file.write(f" ip address 10.{octet}.{link_configured}.2/30\n")
				leaf_zebra_file.close()

				spine_zebra_file.write(f"interface {if_name_spine}\n")
				spine_zebra_file.write(f" ip address 10.{octet}.{link_configured}.1/30\n")
                
				spine_bgpd_file.write(f" neighbor {if_name_spine} interface peer-group TOR\n")
				
				link_configured = link_configured + 1
			spine_zebra_file.close()
			spine_bgpd_file.close()

	link_configured = 0
	octet = 0
	for x in range(0, topo.iLeafLayerRouter):
		router = net.getNodeByName(topo.LeafRouterList[x])
		router.cmd("echo 1 > /proc/sys/net/ipv4/ip_forward")
		leaf_bgpd_file = open(f"{home_dir}/conf/bgpd_" + router.name + ".conf","a")
		zebra_file = open(f"{home_dir}/conf/zebra_" + router.name + ".conf","a")
		leaf_bgpd_file.write("\naddress-family ipv4 unicast\n")
		leaf_bgpd_file.write("  neighbor TOR activate\n")

		if (topo.density == 0):
			if(link_configured>=254):
					octet += 1
					link_configured = 0
			leaf_bgpd_file.write(f" network 200.{str(octet)}.{str(link_configured)}.0/24\n")

			topo.prefixes[topo.LeafRouterList[x]] = f"200.{str(octet)}.{str(link_configured)}.0/24"
			link_configured = link_configured + 1
		else:
			for i in range(0, topo.density):
				if(link_configured>=254):
					octet += 1
					link_configured = 0
				router.cmd("ifconfig %s-eth%s 200.%s.%s.1/24" % (router.name, i + topo.density+1, str(octet) , str(link_configured)))
				leaf_bgpd_file.write(f" network 200.{str(octet)}.{str(link_configured)}.0/24\n")

				zebra_file.write("interface %s-eth%s\n"% (router.name, i + topo.density+1))
				zebra_file.write(" ip address 200.%s.%s.1/24\n" % (str(octet) , str(link_configured)))

				host = net.getNodeByName(topo.HostList[link_configured])
				host.cmd("ifconfig %s-eth0 200.%s.%s.100/24" % (host.name, str(octet), str(link_configured)))
				host.cmd("ip route add default via 200.%s.%s.1" % (str(octet), str(link_configured)))
				link_configured = link_configured + 1
		zebra_file.close()
		leaf_bgpd_file.close()
		
def startDaemons(net,topo):
	for x in range(0, topo.iCoreLayerRouter):
		router = net.getNodeByName(topo.CoreRouterList[x])
		router.cmd(f"/usr/lib/frr/zebra -f {home_dir}/conf/zebra_%s.conf -d -i /tmp/zebra_%s.pid > logs/%s_zebra.stdout 2>&1" % (router.name, router.name, router.name))
		router.waitOutput()
		router.cmd(f"/usr/lib/frr/bgpd --ecmp 64 -f {home_dir}/conf/bgpd_%s.conf -d -i /tmp/bgpd_%s.pid --log file:logs/%s_bgpd.stdout" % (router.name, router.name, router.name), shell=True)
		router.waitOutput()
	
	showMessage("End Cores")

	for x in range(0, topo.iSpineLayerRouter):
		router = net.getNodeByName(topo.SpineRouterList[x])
		router.cmd(f"/usr/lib/frr/zebra -f {home_dir}/conf/zebra_%s.conf -d -i /tmp/zebra_%s.pid > logs/%s_zebra.stdout 2>&1" % (router.name, router.name, router.name))
		router.waitOutput()
		router.cmd(f"/usr/lib/frr/bgpd --ecmp 64 -f {home_dir}/conf/bgpd_%s.conf -d -i /tmp/bgpd_%s.pid --log file:logs/%s_bgpd.stdout" % (router.name, router.name, router.name), shell=True)
		router.waitOutput()

	showMessage("End Spines")

	for x in range(0, topo.iLeafLayerRouter):
		router = net.getNodeByName(topo.LeafRouterList[x])
		router.cmd(f"/usr/lib/frr/zebra -f {home_dir}/conf/zebra_%s.conf -d -i /tmp/zebra_%s.pid > logs/%s_zebra.stdout 2>&1" % (router.name, router.name, router.name))
		router.waitOutput()
		router.cmd(f"/usr/lib/frr/bgpd --ecmp 64 -f {home_dir}/conf/bgpd_%s.conf -d -i /tmp/bgpd_%s.pid --log file:logs/%s_bgpd.stdout" % (router.name, router.name, router.name), shell=True)
		router.waitOutput()

	showMessage("End Leaves")

def createBGPZebraHeaderFiles(net,topo):
	end = int(topo.pod/2)
	asn_nr = 65534
	for x in range(0, topo.iCoreLayerRouter):
		topo.CoreRouterListASNs.append(str(asn_nr))
		router = net.getNodeByName(topo.CoreRouterList[x])

		file = open(f"{home_dir}/conf/bgpd_" + router.name + ".conf","w")
		file.write("hostname " + router.name + "\n")
		file.write("password en\n")
		file.write("enable password en\n")
		file.write("log file /tmp/" + router.name + "-bgpd.log\n")
		file.write("ip prefix-list DC_LOCAL_SUBNET seq 5 permit 10.0.0.0/8 le 30\n")
		file.write("ip prefix-list DC_LOCAL_SUBNET seq 10 permit 200.0.0.0/8 le 24\n")
		file.write("route-map ACCEPT_DC_LOCAL permit 10\n")
		file.write(" match ip address prefix-list DC_LOCAL_SUBNET\n\n")
		file.write("router bgp " + str(asn_nr) + "\n")
		file.write(" timers bgp 3 9\n")
		file.write(f" bgp router-id 192.168.254.{x}\n")
		file.write(" no bgp ebgp-requires-policy\n")
		file.write(" bgp bestpath as-path multipath-relax\n")
		file.write(" bgp bestpath compare-routerid\n")
		file.write(" bgp log-neighbor-changes\n\n")
		file.write("neighbor fabric peer-group\n")
		file.write(" neighbor fabric remote-as external\n")
		file.write(" neighbor fabric advertisement-interval 0\n")
		file.write(" neighbor fabric timers connect 5\n")
		file.close()

		file = open(f"{home_dir}/conf/zebra_" + router.name + ".conf","w")
		file.write("hostname " + router.name + "\n")
		file.write("password en\n")
		file.write("enable password en\n")
		file.write("log file /tmp/" + router.name + "-zebra.log\n")
		file.close()
	
	id = 0
	octet = 0
	spinesASN = list(range(0, int(k**2/2 + k), int(k/2)+1))

	for x in range(0, topo.iSpineLayerRouter):
		pod_nr = int(x / end)
		asn_nr = 65000 + spinesASN[pod_nr]
		topo.SpineRouterListASNs.append(str(asn_nr))
		router = net.getNodeByName(topo.SpineRouterList[x])

		file = open(f"{home_dir}/conf/bgpd_{router.name}.conf","w")
		file.write(f"hostname {router.name}\n")
		file.write("password en\n")
		file.write("enable password en\n")
		file.write(f"log file /tmp/{router.name}-bgpd.log\n")
		file.write("ip prefix-list DC_LOCAL_SUBNET seq 5 permit 10.0.0.0/8 le 30\n")
		file.write("ip prefix-list DC_LOCAL_SUBNET seq 10 permit 200.0.0.0/8 le 24\n")
		file.write("route-map ACCEPT_DC_LOCAL permit 10\n")
		file.write(" match ip address prefix-list DC_LOCAL_SUBNET\n\n")

		file.write("router bgp " + str(asn_nr) + "\n")
		if id > 254:
			id=0
			octet+=1

		file.write(" timers bgp 3 9\n")
		file.write(f" bgp router-id 192.168.{octet}.{id}\n")
		file.write(" no bgp ebgp-requires-policy\n")
		file.write(" bgp bestpath as-path multipath-relax\n")
		file.write(" bgp bestpath compare-routerid\n")
		file.write(" bgp log-neighbor-changes\n\n")
		file.write("neighbor TOR peer-group\n")
		file.write(" neighbor TOR remote-as external\n")
		file.write(" neighbor TOR advertisement-interval 0\n")
		file.write(" neighbor TOR timers connect 5\n")
		file.write("neighbor fabric peer-group\n")
		file.write(" neighbor fabric remote-as external\n")
		file.write(" neighbor fabric advertisement-interval 0\n")
		file.write(" neighbor fabric timers connect 5\n")
		file.close()

		file = open(f"{home_dir}/conf/zebra_{router.name}.conf","w")
		file.write(f"hostname {router.name}\n")
		file.write("password en\n")
		file.write("enable password en\n")
		file.write(f"log file /tmp/{router.name}-zebra.log\n")
		file.close()
		id+=1

	for x in range(0, topo.iLeafLayerRouter, end):
		pod_nr = int(x / end)
		for i in range(0, end):
			asn_nr = 65000 + spinesASN[pod_nr] + i + 1
			topo.LeafRouterListASNs.append(str(asn_nr))
			router = net.getNodeByName(topo.LeafRouterList[x + i])

			file = open(f"{home_dir}/conf/bgpd_{router.name}.conf","w")
			file.write(f"hostname {router.name}\n")
			file.write("password en\n")
			file.write("enable password en\n")
			file.write(f"log file /tmp/{router.name}-bgpd.log\n")
			file.write("ip prefix-list DC_LOCAL_SUBNET seq 5 permit 10.0.0.0/8 le 30\n")
			file.write("ip prefix-list DC_LOCAL_SUBNET seq 10 permit 200.0.0.0/8 le 24\n")
			file.write("route-map ACCEPT_DC_LOCAL permit 10\n")
			file.write(" match ip address prefix-list DC_LOCAL_SUBNET\n\n")

			if id > 254:
				id=0
				octet+=1
			file.write("router bgp " + str(asn_nr) + "\n")
			file.write(" timers bgp 3 9\n")
			file.write(f" bgp router-id 192.168.{octet}.{id}\n")
			file.write(" no bgp ebgp-requires-policy\n")
			file.write(" bgp bestpath as-path multipath-relax\n")
			file.write(" bgp bestpath compare-routerid\n")
			file.write(" bgp log-neighbor-changes\n\n")
			file.write("neighbor TOR peer-group\n")
			file.write(" neighbor TOR remote-as external\n")
			file.write(" neighbor TOR advertisement-interval 0\n")
			file.write(" neighbor TOR timers connect 5\n")
			file.close()

			file = open(f"{home_dir}/conf/zebra_{router.name}.conf","w")
			file.write(f"hostname {router.name}\n")
			file.write("password en\n")
			file.write("enable password en\n")
			file.write(f"log file /tmp/{router.name}-zebra.log\n")
			file.close()
			id+=1

def createBGPZebraFooterFiles(net,topo):
	for x in range(0, topo.iCoreLayerRouter):
		router = net.getNodeByName(topo.CoreRouterList[x])
		file = open(f"{home_dir}/conf/bgpd_{router.name}.conf","a")
		file.write("\naddress-family ipv4 unicast\n")
		file.write(" neighbor fabric activate\n")
		file.write(" maximum-paths 64\n")
		file.write("exit-address-family\n")
		file.write("log stdout\n")
		file.close()

	for x in range(0, topo.iSpineLayerRouter):
		router = net.getNodeByName(topo.SpineRouterList[x])
		file = open(f"{home_dir}/conf/bgpd_{router.name}.conf","a")
		file.write("\naddress-family ipv4 unicast\n")
		file.write(" neighbor fabric activate\n")
		file.write(" neighbor TOR activate\n")
		file.write(" maximum-paths 64\n")
		file.write("exit-address-family\n")
		file.write("log stdout\n")
		file.close()

	for x in range(0, topo.iLeafLayerRouter):
		router = net.getNodeByName(topo.LeafRouterList[x])
		file = open(f"{home_dir}/conf/bgpd_{router.name}.conf","a")
		file.write(" maximum-paths 64\n")
		file.write("exit-address-family\n")
		file.write("log stdout\n")
		file.close()

def showMessage(message):
	logger.debug(message)
	sys.stdout.write(message + "\n")
	sys.stdout.flush

def stopFlag(net,topo):
	showMessage("TODO")

def preCalculatedTables(topo):
	for sw in topo.switches():
		topo.paths[sw] = []
		for leaf in topo.LeafRouterList:
			if leaf != sw:
				paths = nx.all_shortest_paths(topo.graph, source=sw, target=leaf)
				q = [0,0,0]
				for p in paths:
					if(p[len(p)-1] != q[len(q)-1] or p[1] != q[1]):
						topo.paths[sw].append(p)
					q = p

def dumpExpectedTable(topo):
	file = open(f"{home_dir}/logs/expected_tables.log","a")
	for sw in topo.switches():
		file.write(f"{sw}\n")
		file.write(f"{topo.paths[sw]}\n")
	file.close()


def checkConvergenceTables(net, topo):
	remain_s = topo.switches()
	round = 0
	while len(remain_s) > 0:
		round+=1
		routers = remain_s.copy()
		print(f"Checking convergence... round: {round}, {len(routers)} left")
		for r in routers:
			router = net.getNodeByName(r)
			table = router.cmd(f"ip --json route") #actual table
			router.waitOutput()
			table = json.loads(table)
			remain_p = topo.paths[r].copy() #expected routes
			it = len(remain_p)-1
			while len(remain_p) > 0 and it>=0:
				path = remain_p[0]
				dst = topo.prefixes[path[len(path)-1]]
				actual_paths_to_dst = [prefix for prefix in table if prefix["dst"] == dst]
				if len(actual_paths_to_dst) == 0:
					break
				if 'nexthops' in actual_paths_to_dst[0]:
					l_actual_paths_to_dst = len(actual_paths_to_dst[0]['nexthops'])
				else:
					l_actual_paths_to_dst = 1
				expected_paths_to_dst = [route for route in remain_p if route[len(route)-1] == path[len(path)-1]]
				if l_actual_paths_to_dst == len(expected_paths_to_dst):
					for p in expected_paths_to_dst:
						remain_p.remove(p)
				else:
					break
				it -= 1
			if len(remain_p)==0:
				remain_s.remove(r)
		time.sleep(k)
		if round >= 40:
			print(f"Fail. {len(remain_s)} left. {remain_s}")
			logger.debug(f"Fail. {len(remain_s)} left. {remain_s}")
			return -1
			
	return round



def dumpTables(net,topo):
	showMessage("Dumping forwarding tables")
	for x in range(0, topo.iCoreLayerRouter):
		router = net.getNodeByName(topo.CoreRouterList[x])
		output = router.cmd("ip route")
		file = open(f"{home_dir}/outputs/table_" + router.name + ".txt","w")
		file.write(output)
		file.close()

	for x in range(0, topo.iSpineLayerRouter):
		router = net.getNodeByName(topo.SpineRouterList[x])
		output = router.cmd("ip route")
		file = open(f"{home_dir}/outputs/table_" + router.name + ".txt","w")
		file.write(output)
		file.close()

	for x in range(0, topo.iLeafLayerRouter):
		router = net.getNodeByName(topo.LeafRouterList[x])
		output = router.cmd("ip route")
		file = open(f"{home_dir}/outputs/table_" + router.name + ".txt","w")
		file.write(output)
		file.close()
	
	showMessage("End dumping forwarding tables")

	#Use cases
def run_bootstrap(net, topo):	
	print("Running bootstrap...")
	time.sleep(k**2)
	# if hosts>0:	
	# 	dropped = net.pingAll()
	# 	if (dropped == 0):
	# 		print("OK BOOTSTRAP")
	# 	else:
	# 		print("NOT FULL CONNECTIVITY AT BOOTSTRAP")


def run_node_failure(net, topo):
	print(f"running node_failure")
	net.delLinkBetween (net.getNodeByName('r30001'), net.getNodeByName('r20001'), allLinks = True)
	net.delLinkBetween (net.getNodeByName('r30001'), net.getNodeByName('r20002'), allLinks = True)
	topo.graph.remove_edge("r30001", "r20001")
	topo.graph.remove_edge("r30001", "r20002")
	if hosts>0:
		net.delLinkBetween (net.getNodeByName('r30001'), net.getNodeByName('h1'), allLinks = True)
		net.delLinkBetween (net.getNodeByName('r30001'), net.getNodeByName('h2'), allLinks = True)
	print("r30001 down")

def run_node_recovery(net, topo):
	print(f"Running Node Recovery test...")
	run_node_failure(net, topo)
	net.addLink (net.getNodeByName('r30001'), net.getNodeByName('r20001'))
	net.addLink (net.getNodeByName('r30001'), net.getNodeByName('r20002'))
	topo.graph.add_edge("r30001", "r20001")
	topo.graph.add_edge("r30001", "r20002")
	if hosts>0:
		net.addLink (net.getNodeByName('r30001'), net.getNodeByName('h1'))
		net.addLink (net.getNodeByName('r30001'), net.getNodeByName('h2'))
	print("r30001 up")
	time.sleep(k**2)
	# if hosts>0:
	# 	dropped = net.pingAll()
	# 	if (dropped == 0):
	# 		print("OK Node Recovery")
	# 	else:
	# 		print("Node Recovery: fails")

def run_link_failure(net, topo):
	print(f"running link_failure")	
	net.delLinkBetween (net.getNodeByName('r30001'), net.getNodeByName('r20002'), allLinks = True)
	print("link r30001 <-> r20002 down")
	topo.graph.remove_edge("r30001", "r20002")
	time.sleep(k**2)
	# if hosts>0:
	# 	dropped = net.pingAll()
	# 	if (dropped == 0):
	# 		print("OK Link Failure")
	# 	else:
	# 		print("Link Failure: fails")

def run_link_recovery(net, topo):	
	print(f"Running Link Recovery")	
	# net.delLinkBetween (net.getNodeByName('r30001'), net.getNodeByName('r20002'), allLinks = True)
	# print("link r30001 <-> r20002 down")
	# topo.graph.remove_edge("r30001", "r20002")
	# time.sleep(k**2)
	run_link_failure(net, topo)
	net.addLink (net.getNodeByName('r30001'), net.getNodeByName('r20002'))
	print("link r30001 <-> r20002 up")
	topo.graph.add_edge("r30001", "r20002")
	# if hosts>0:
	# 	dropped = net.pingAll()
	# 	if (dropped == 0):
	# 		print("OK Link Recovery")
	# 	else:
	# 		print("Link Recovery: fails")

def run_partitioned_fabric(net, topo):
	print(f"Running Partitioned Fabric")	
	net.delLinkBetween (net.getNodeByName('r10001'), net.getNodeByName('r20002'), allLinks = True)
	print("link r10001 <-> r20002 down")
	topo.graph.remove_edge("r10001", "r20002")
	time.sleep(k**2)
	# if hosts>0:
	# 	dropped = net.pingAll()
	# 	if (dropped == 0):
	# 		print("OK Partitioned Fabric")
	# 	else:
	# 		print("Partitioned Fabric: fails")

def run_partitioned_fabric_plane(net, topo):	
	print(f"running partitioned_fabric_plane")	
	net.delLinkBetween (net.getNodeByName('r10001'), net.getNodeByName('r20001'), allLinks = True)
	print("link r10001 <-> r20001 down")
	topo.graph.remove_edge("r10001", "r20001")
	net.delLinkBetween (net.getNodeByName('r10002'), net.getNodeByName('r20001'), allLinks = True)
	print("link r10002 <-> r20001 down")
	topo.graph.remove_edge("r10002", "r20001")
	time.sleep(k**2)
	# if hosts>0:
	# 	dropped = net.pingAll()
	# 	if (dropped == 0):
	# 		print("OK Partitioned Fabric Plane")
	# 	else:
	# 		print("Partitioned Fabric Plane: fails")




def main(k):
	os.system(f"rm -f /tmp/*.pid {home_dir}/logs/*.stdout {home_dir}/logs/*.log /tmp/*.log")
	os.system("killall -9 bgpd zebra > /dev/null 2>&1")
	
	if not os.path.exists(f'{home_dir}/conf'):
		os.makedirs(f'{home_dir}/conf')
	else:
		os.system(f"rm -f {home_dir}/conf/*")
	
	if not os.path.exists(f'{home_dir}/logs'):
		os.makedirs(f'{home_dir}/logs')

	if not os.path.exists(f'{home_dir}/outputs'):
		os.makedirs(f'{home_dir}/outputs')
	else:
		os.system(f"rm -f {home_dir}/outputs/*")
	
	if not os.path.exists(f'{home_dir}/pcaps'):
		os.makedirs(f'{home_dir}/pcaps')
	else:
		os.system(f"rm -f {home_dir}/pcaps/*")
	
	topo=FatTree(k)
	net = Mininet(topo)
	
	createBGPZebraHeaderFiles(net,topo)
	showMessage("BGP and Zebra config Files created!")
	
	configRouterInterfaces(net,topo)
	showMessage("Router interfaces configured!")
	
	createBGPZebraFooterFiles(net,topo)
	showMessage("BGP and Zebra config footer added!")
	
	startDaemons(net,topo)
	showMessage("Deamons started!")
	
	showMessage("*** Starting network")

	showMessage("*** Creating graph")

	# topo.graph = nx.Graph()
	# topo.graph.add_edges_from(topo.links())
	
	#stopFlag(net, topo)
	#preCalculatedTables(net,topo)
	# time.sleep(3600)
	#run use cases, uncomment the tests you want to perform
	#run_partitioned_fabric_plane(net)
	#run_partitioned_fabric(net)
	#run_link_failure(net, topo)
	#time.sleep(6)

	# showMessage("*** Precalculating routing tables from graph")
	# preCalculatedTables(topo)

	# showMessage("*** Checking convergence at boostrap")
	# attempt = checkConvergenceTables(net,topo)
	# if attempt > 0:
	# 	print(f"The protocol has converged on the attempt {attempt}")
	# else:
	# 	print(f"Max attempts reached. The protocol has not converged")

	#run_link_recovery(net)
	#run_node_failure(net)
	#run_node_recovery(net)

	# showMessage("*** LEAFS ASNs")
	# for i in topo.LeafRouterListASNs :
	# 	print(i)
	# showMessage("*** SPINE ASNs")
	# for x in topo.SpineRouterListASNs :
	# 	print(x)

	CLI(net)
	dumpTables(net, topo)
	# dumpExpectedTable(topo)
	net.stop()

	showMessage("*** Stopping network")
	os.system("killall -9 bgpd zebra > /dev/null 2>&1")

if __name__ == "__main__":
	if len(sys.argv) < 2:
		print ('fattree_isis.py k <hosts per leaf>')
		sys.exit(2)
	
	k = int(sys.argv[1])
	hosts = -1
	
	if k & 1:
		print ('k must be even')
		sys.exit(2)

	if len(sys.argv) > 2:
		hosts = int(sys.argv[2])

	if hosts > k/2:
		print ('hosts must be <= k/2')
		sys.exit(2)

	main(k)
