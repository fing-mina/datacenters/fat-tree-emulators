from types import ClassMethodDescriptorType
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.cli import CLI
import logging
import os
import sys

logging.basicConfig(filename='logs/fattree_topo.log', level=logging.DEBUG)
logger = logging.getLogger(__name__)

class FatTree( Topo ):
  
	CoreRouterList = []
	CoreRouterListASNs = []
	AggRouterList = []
	AggRouterListASNs = []
	EdgeRouterList = []
	EdgeRouterListASNs = []
	HostList = []

	def __init__( self, k ):

		"Create Fat Tree topo."
		self.pod = k
		self.iCoreLayerRouter = int((k/2)**2)
		self.iAggLayerRouter = int(k*k/2)
		self.iEdgeLayerRouter = int(k*k/2)
		
		if hosts < 0:
			self.density = int(k/2)
		else:
			self.density = hosts

		self.iHost = int(self.iEdgeLayerRouter * self.density)

		
		showMessage("self.pod: " + str(self.pod))
		showMessage("self.iCoreLayerRouter: " + str(self.iCoreLayerRouter))
		showMessage("self.iAggLayerRouter: " + str(self.iAggLayerRouter))
		showMessage("self.iEdgeLayerRouter: " + str(self.iEdgeLayerRouter))
		showMessage("self.density: " + str(self.density))
		showMessage("self.iHost: " + str(self.iHost))
		
		self.bw_c2a = 0.2
		self.bw_a2e = 0.1
		self.bw_h2a = 0.05

		# Init Topo
		Topo.__init__(self)
  
		self.createTopo()
		showMessage("Finished topology creation!")

		self.createLink( bw_c2a=self.bw_c2a, 
						 bw_a2e=self.bw_a2e, 
						 bw_h2a=self.bw_h2a)
		showMessage("Finished adding links!")
		
	def createTopo(self):
		self.createCoreLayerRouter(self.iCoreLayerRouter)
		self.createAggLayerRouter(self.iAggLayerRouter)
		self.createEdgeLayerRouter(self.iEdgeLayerRouter)
		self.createHost(self.iHost)

	"""
	Create Router and Host
	"""

	def _addRouter(self, number, level, Router_list):
		for x in range(1, number+1):
			PREFIX = str(level) + "000"
			if x >= int(10):
				PREFIX = str(level) + "00"
			if x >= int(100):
				PREFIX = str(level) + "0"
			Router_list.append(self.addHost('r' + PREFIX + str(x)))

	def createCoreLayerRouter(self, NUMBER):
		showMessage("Create Core Layer")
		self._addRouter(NUMBER, 1, self.CoreRouterList)

	def createAggLayerRouter(self, NUMBER):
		showMessage("Create Agg Layer")
		self._addRouter(NUMBER, 2, self.AggRouterList)

	def createEdgeLayerRouter(self, NUMBER):
		showMessage("Create Edge Layer")
		self._addRouter(NUMBER, 3, self.EdgeRouterList)

	def createHost(self, NUMBER):
		showMessage("Create Host")
		for x in range(1, NUMBER+1):
			PREFIX = "h000"
			if x >= int(10):
				PREFIX = "h00"
			if x >= int(100):
				PREFIX = "h0"
			if x >= int(1000):
				PREFIX = "h"
			self.HostList.append(self.addHost(PREFIX + str(x)))

	"""
	Add Link
	"""
	def createLink(self, bw_c2a=0.2, bw_a2e=0.1, bw_h2a=0.5):
		showMessage("Add link Core to Agg.")
		end = int(self.pod/2)
		for x in range(0, self.iAggLayerRouter, end):
			for i in range(0, end):
				for j in range(0, end):
					linkopts = dict(bw=bw_c2a) 
					self.addLink(
						self.CoreRouterList[i*end+j],
						self.AggRouterList[x+i],
						**linkopts)

		showMessage("Add link Agg to Edge.")
		for x in range(0, self.iAggLayerRouter, end):
			for i in range(0, end):
				for j in range(0, end):
					linkopts = dict(bw=bw_a2e) 
					self.addLink(
						self.AggRouterList[x+i],
						self.EdgeRouterList[x+j],
						**linkopts)

		showMessage("Add link Edge to Host.")
		for x in range(0, self.iEdgeLayerRouter):
			for i in range(0, self.density):
				linkopts = dict(bw=bw_h2a) 
				self.addLink(
					self.EdgeRouterList[x],
					self.HostList[self.density * x + i],
					**linkopts)
	
def configRouterInterfaces(net,topo):
	end = int(topo.pod/2)
	octet = 0
	link_configured = 0
	for x in range(0, topo.iAggLayerRouter, end):
		core_interface_index = int(x / end)
		for i in range(0, end):
			agg_router = net.getNodeByName(topo.AggRouterList[x+i])
			agg_router.cmd("echo 1 > /proc/sys/net/ipv4/ip_forward")
			for j in range(0, end):
				if(link_configured>254):
					octet += 1
					link_configured = 0
				agg_interface_index = j
				core_router = net.getNodeByName(topo.CoreRouterList[i*end+j])
				core_router.cmd("echo 1 > /proc/sys/net/ipv4/ip_forward")
				core_router.cmd("ifconfig %s-eth%s 10.%s.%s.1/24" % (core_router.name, core_interface_index, octet, link_configured))
				agg_router.cmd("ifconfig %s-eth%s 10.%s.%s.2/24" % (agg_router.name, agg_interface_index, octet, link_configured))
				link_configured = link_configured + 1

	link_configured = 0
	octet = 10
	for x in range(0, topo.iAggLayerRouter, end):
		for i in range(0, end):
			edge_interface_index = i
			agg_router = net.getNodeByName(topo.AggRouterList[x+i])
			for j in range(0, end):
				if(link_configured>254):
					octet += 1
					link_configured = 0
				agg_interface_index = j + end
				edge_router = net.getNodeByName(topo.EdgeRouterList[x+j])
				edge_router.cmd("ifconfig %s-eth%s 10.%s.%s.2/24" % (edge_router.name, edge_interface_index, octet, link_configured))
				agg_router.cmd("ifconfig %s-eth%s 10.%s.%s.1/24" % (agg_router.name, agg_interface_index, octet, link_configured))
				link_configured = link_configured + 1

	link_configured = 0
	octet = 16
	for x in range(0, topo.iEdgeLayerRouter):
		router = net.getNodeByName(topo.EdgeRouterList[x])
		router.cmd("echo 1 > /proc/sys/net/ipv4/ip_forward")
		for i in range(0, topo.density):
			if(link_configured>254):
				octet += 1
				link_configured = 0
			router.cmd("ifconfig %s-eth%s 172.%s.%s.1/24" % (router.name, i + topo.density, str(octet) , str(link_configured)))
			host = net.getNodeByName(topo.HostList[link_configured])
			host.cmd("ifconfig %s-eth0 172.%s.%s.100/24" % (host.name, str(octet), str(link_configured)))
			host.cmd("ip route add default via 172.%s.%s.1" % (str(octet), str(link_configured)))
			link_configured = link_configured + 1

def showMessage(message):
	logger.debug(message)
	sys.stdout.write(message + "\n")
	sys.stdout.flush

def main(k):
	os.system("rm -f /tmp/*.pid logs/*.stdout")
	os.system("killall -9 bgpd zebra > /dev/null 2>&1")
	
	topo=FatTree(k)
	net = Mininet(topo)
	
	configRouterInterfaces(net,topo)
	showMessage("Router interfaces configured!")
	
	showMessage("*** Starting network")
	
	CLI(net)
	net.stop()


if __name__ == "__main__":
	if len(sys.argv) < 2:
		print ('fattree_isis.py k <hosts per leaf>')
		sys.exit(2)
	
	k = int(sys.argv[1])
	hosts = -1
	
	if k & 1:
		print ('k must be even')
		sys.exit(2)

	if len(sys.argv) > 2:
		hosts = int(sys.argv[2])

	if hosts > k/2:
		print ('hosts must be <= k/2')
		sys.exit(2)

	main(k)