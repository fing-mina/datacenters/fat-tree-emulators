from mininet.topo import Topo
from mininet.net import Mininet
from mininet.cli import CLI
import logging
import os
import sys

path_yaml =  "/home/mininet/pruebas/mininet/fattree/conf-rift/"
path_rift = "/home/mininet/rift-python/"
logger = logging.getLogger(__name__)


class FatTree( Topo ):
  
	CoreRouterList = []
	SpineRouterList = []
	LeafRouterList = []
	HostList = []

	def __init__( self, k):

		"Create Fat Tree topology"
		self.pod = int(k)
		self.iCoreLayerRouter = int((k/2)**2)
		self.iSpineLayerRouter = int(k*k/2)
		self.iLeafLayerRouter = int(k*k/2)
		
		if hosts < 0:
			self.density = int(k/2)
		else:
			self.density = hosts
			
		self.iHost = int(self.iLeafLayerRouter * self.density)
		
		showMessage("hosts: " + str(hosts))
		showMessage("self.pod: " + str(self.pod))
		showMessage("self.iCoreLayerRouter: " + str(self.iCoreLayerRouter))
		showMessage("self.iSpineLayerRouter: " + str(self.iSpineLayerRouter))
		showMessage("self.iLeafLayerRouter: " + str(self.iLeafLayerRouter))
		showMessage("self.density: " + str(self.density))
		showMessage("self.iHost: " + str(self.iHost))
		
		self.bw_c2a = 0.2
		self.bw_a2e = 0.1
		self.bw_h2a = 0.05

		# Init Topo
		Topo.__init__(self)
  
		self.createTopo()
		showMessage("Finished topology creation!")

		self.createLink( bw_c2a=self.bw_c2a, 
						 bw_a2e=self.bw_a2e, 
						 bw_h2a=self.bw_h2a)
		showMessage("Finished adding links!")
		
	def createTopo(self):
		self.createCoreLayerRouter(self.iCoreLayerRouter)
		self.createSpineLayerRouter(self.iSpineLayerRouter)
		self.createLeafLayerRouter(self.iLeafLayerRouter)
		self.createHost(self.iHost)

	"""
	Create Router and Host
	"""

	def _addRouter(self, number, level, Router_list):
		for x in range(1, number+1):
			PREFIX = str(level) + "000"
			if x >= int(10):
				PREFIX = str(level) + "00"
			if x >= int(100):
				PREFIX = str(level) + "0"
			Router_list.append(self.addHost('r' + PREFIX + str(x)))

	def createCoreLayerRouter(self, NUMBER):
		showMessage("Create Core Layer")
		self._addRouter(NUMBER, 1, self.CoreRouterList)

	def createSpineLayerRouter(self, NUMBER):
		showMessage("Create Spine Layer")
		self._addRouter(NUMBER, 2, self.SpineRouterList)

	def createLeafLayerRouter(self, NUMBER):
		showMessage("Create Leaf Layer")
		self._addRouter(NUMBER, 3, self.LeafRouterList)

	def createHost(self, NUMBER):
		showMessage("Create Host")
		for x in range(1, NUMBER+1):
			PREFIX = "h000"
			if x >= int(10):
				PREFIX = "h00"
			if x >= int(100):
				PREFIX = "h0"
			if x >= int(1000):
				PREFIX = "h"
			self.HostList.append(self.addHost(PREFIX + str(x)))

	"""
	Add Link
	"""
	def createLink(self, bw_c2a=0.2, bw_a2e=0.1, bw_h2a=0.5):
		showMessage("Add link Core to Spine.")
		end = int(self.pod/2)
		for x in range(0, self.iSpineLayerRouter, end):
			for i in range(0, end):
				for j in range(0, end):
					linkopts = dict(bw=bw_c2a) 
					self.addLink(
						self.CoreRouterList[i*end+j],
						self.SpineRouterList[x+i],
						**linkopts)

		showMessage("Add link Spine to Leaf.")
		for x in range(0, self.iSpineLayerRouter, end):
			for i in range(0, end):
				for j in range(0, end):
					linkopts = dict(bw=bw_a2e) 
					self.addLink(
						self.SpineRouterList[x+i],
						self.LeafRouterList[x+j],
						**linkopts)

		showMessage("Add link Leaf to Host.")
		for x in range(0, self.iLeafLayerRouter):
			for i in range(0, self.density):
				linkopts = dict(bw=bw_h2a) 
				self.addLink(
					self.LeafRouterList[x],
					self.HostList[self.density * x + i],
					**linkopts)
	
def configRouterInterfaces(net,topo):
	end = int(topo.pod/2)
	link_configured = 0
	octet = 0
	for x in range(0, topo.iSpineLayerRouter, end):
		core_interface_index = int(x / end)
		for i in range(0, end):
			spine_router = net.getNodeByName(topo.SpineRouterList[x+i])
			spine_router.cmd("echo 1 > /proc/sys/net/ipv4/ip_forward")
			spine_rift_file = open(path_yaml+ spine_router.name + ".yaml","a")
			for j in range(0, end):
				if(link_configured>254):
					octet += 1
					link_configured = 0
				spine_interface_index = j
				core_router = net.getNodeByName(topo.CoreRouterList[i*end+j])
				core_router.cmd("echo 1 > /proc/sys/net/ipv4/ip_forward")
				core_rift_file = open(path_yaml+ core_router.name + ".yaml","a")
				try:
					intf_local = "%s-eth%s " % (core_router.name, core_interface_index)
					intf_peer = "%s-eth%s" % (spine_router.name, spine_interface_index)

					core_router.cmd("ifconfig "+ intf_local +" 10.%s.%s.1/24" % (octet, link_configured))
					core_rift_file.write("          - name: "+ intf_local +"\n")
					description = "{} -> {}".format(intf_local, intf_peer)
					core_rift_file.write("            # " + description +"\n")

					intf_peer = "%s-eth%s" % (spine_router.name, spine_interface_index)
					spine_router.cmd("ifconfig "+ intf_peer +" 10.%s.%s.2/24" % (octet, link_configured))
					spine_rift_file.write("          - name: "+ intf_peer +"\n")
					description = "{} -> {}".format(intf_peer, intf_local)
					spine_rift_file.write("            # " + description +"\n")
				finally:
					core_rift_file.close()
				link_configured = link_configured + 1	
			spine_rift_file.close()
			

	for x in range(0, topo.iSpineLayerRouter, end):
		for i in range(0, end):
			leaf_interface_index = i
			spine_router = net.getNodeByName(topo.SpineRouterList[x+i])
			spine_rift_file = open(path_yaml + spine_router.name + ".yaml","a")
			for j in range(0, end):
				if(link_configured>254):
					octet += 1
					link_configured = 0
				spine_interface_index = j + end
				leaf_router = net.getNodeByName(topo.LeafRouterList[x+j])
				leaf_rift_file = open(path_yaml + leaf_router.name + ".yaml","a")
				try:
					intf_local = "%s-eth%s " % (leaf_router.name, leaf_interface_index)
					intf_peer = "%s-eth%s" % (spine_router.name, spine_interface_index)

					leaf_router.cmd("ifconfig "+ intf_local +" 10.%s.%s.2/24" % (octet, link_configured))
					leaf_rift_file.write("          - name: "+ intf_local +"\n")
					description = "{} -> {}".format(intf_local, intf_peer)
					leaf_rift_file.write("            # " + description +"\n")

					spine_router.cmd("ifconfig "+ intf_peer +" 10.%s.%s.1/24" % (octet, link_configured))
					spine_rift_file.write("          - name: "+ intf_peer +"\n")
					description = "{} -> {}".format(intf_peer, intf_local)
					spine_rift_file.write("            # " + description +"\n")
				finally:
					leaf_rift_file.close()
     
				link_configured = link_configured + 1
			spine_rift_file.close()
			

	link_configured = 0
	octet = 16
	for x in range(0, topo.iLeafLayerRouter):
		leaf_router = net.getNodeByName(topo.LeafRouterList[x])
		leaf_router.cmd("echo 1 > /proc/sys/net/ipv4/ip_forward")
		leaf_rift_file = open(path_yaml + leaf_router.name + ".yaml","a")
		for i in range(0, topo.density):
			if(link_configured>254):
				octet += 1
				link_configured = 0
			host = net.getNodeByName(topo.HostList[link_configured])

			intf_local = "%s-eth%s " % (leaf_router.name, i + topo.density)
			intf_peer = "%s-eth0" % (host.name)
			
			leaf_router.cmd("ifconfig "+ intf_local +" 172.%s.%s.1/24" % (str(octet), str(link_configured)))
			
			host.cmd("ifconfig "+ intf_peer +" 172.%s.%s.100/24" % (str(octet), str(link_configured)))
			host.cmd("ip route add default via 172.%s.%s.1" % (str(octet), str(link_configured)))

			link_configured = link_configured + 1
		
		leaf_rift_file.close()
		
def startDaemons(net,topo):
	for x in range(0, topo.iCoreLayerRouter):
		router = net.getNodeByName(topo.CoreRouterList[x])
		router.cmd("%senv/bin/python3 %srift  --ipv6-multicast-loopback-disable --telnet-port-file /tmp/rift-python-telnet-port-%s %s%s.yaml >/dev/null 2>&1 &" % (path_rift, path_rift, router.name, path_yaml, router.name))

	showMessage("End Cores")

	for x in range(0, topo.iSpineLayerRouter):
		router = net.getNodeByName(topo.SpineRouterList[x])
		router.cmd("%senv/bin/python3 %srift  --ipv6-multicast-loopback-disable --telnet-port-file /tmp/rift-python-telnet-port-%s %s%s.yaml >/dev/null 2>&1 &" % (path_rift, path_rift, router.name, path_yaml, router.name))

	showMessage("End Spines")

	for x in range(0, topo.iLeafLayerRouter):
		router = net.getNodeByName(topo.LeafRouterList[x])
		router.cmd("%senv/bin/python3 %srift  --ipv6-multicast-loopback-disable --telnet-port-file /tmp/rift-python-telnet-port-%s %s%s.yaml >/dev/null 2>&1 &" % (path_rift, path_rift, router.name, path_yaml, router.name))

	showMessage("End Leaves")

def createRIFTBaseFiles(net,topo):
	end = int(topo.pod/2)
	for x in range(0, topo.iCoreLayerRouter):
		router = net.getNodeByName(topo.CoreRouterList[x])
		fs = open(path_yaml + router.name + ".yaml","w+")
		try:
			fs.write("shards:\n")
			fs.write("  - id: 0\n")
			fs.write("    nodes:\n")
			fs.write("      - name: " + router.name +  "\n")
			fs.write("        level: 2\n")
			fs.write("        systemid: " + str(x+1) + "\n")
			fs.write("        interfaces:\n")		
		finally:
			fs.close()

	for x in range(0, topo.iSpineLayerRouter):
		router = net.getNodeByName(topo.SpineRouterList[x])
		fs = open(path_yaml+ router.name + ".yaml","w+")
		try:
			fs.write("shards:\n")
			fs.write("  - id: 0\n")
			fs.write("    nodes:\n")
			fs.write("      - name: " + router.name +  "\n")
			fs.write("        level: 1\n")
			fs.write("        systemid: 10" + str(x+1) + "\n")
			fs.write("        interfaces:\n")
		finally:
			fs.close()

	for x in range(0, topo.iLeafLayerRouter, end):
		for i in range(0, end):
			router = net.getNodeByName(topo.LeafRouterList[x + i])
			fs = open(path_yaml+ router.name + ".yaml","w+")
			try:
				fs.write("shards:\n")
				fs.write("  - id: 0\n")
				fs.write("    nodes:\n")
				fs.write("      - name: " + router.name +  "\n")
				fs.write("        level: 0\n")
				fs.write("        systemid: 100" + str(x+i) + "\n")
				fs.write("        interfaces:\n")
			finally:
				fs.close()


def createRIFTFooterFiles(net,topo):
	octet = 16
	link_configured = 0
	metric = 1
	mask = 24
	for x in range(0, topo.iLeafLayerRouter):
		router = net.getNodeByName(topo.LeafRouterList[x])
		fs = open(path_yaml+ router.name + ".yaml","a")
		try:
			fs.write("        v4prefixes:\n")
		finally:
			fs.close()
		for i in range(0, topo.density):
			if(link_configured>254):
				octet += 1
				link_configured = 0
			address1 = "172."+str(octet)+"."+ str(int(topo.density * x + i)) +".0"
			fs = open(path_yaml+ router.name + ".yaml","a")
			try:
				fs.write("          - address: " + address1 +"\n")
				fs.write("            mask: " + str(mask) +"\n")
				fs.write("            metric: " + str(metric) + "\n")
			finally:
				fs.close()
			link_configured+=1


def showMessage(message):
	logger.debug(message)
	sys.stdout.write(message + "\n")
	sys.stdout.flush


def main(k):
	os.system("rm -f *.log")
	os.system("rm -f /tmp/rift*")        
	os.system("rm -f "+ path_yaml+"*.yaml")
	
	topo=FatTree(k)
	net = Mininet(topo)
	
	createRIFTBaseFiles(net,topo)
	showMessage("RIFT Files created!")
	
	configRouterInterfaces(net,topo)
	showMessage("Router interfaces configured!")
	
	createRIFTFooterFiles(net,topo)
	showMessage("RIFT config footers added!")
	
	startDaemons(net,topo)
	showMessage("Deamons started!")
	
	showMessage("*** Starting network")
	
	CLI(net)
	net.stop()

	showMessage("*** Stopping network")

if __name__ == "__main__":
	if len(sys.argv) < 2:
		print ('fattree_isis.py k <hosts per leaf>')
		sys.exit(2)
	
	k = int(sys.argv[1])
	hosts = -1
	
	if k & 1:
		print ('k must be even')
		sys.exit(2)

	if len(sys.argv) > 2:
		hosts = int(sys.argv[2])

	if hosts > k/2:
		print ('hosts must be <= k/2')
		sys.exit(2)

	main(k)