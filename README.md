# Experimentation environments for routing protocols in the datacenter

This repo contains a collection of experimentation environments to work with fat-tree datacenter topologies.


## Usage and Installation

Depending on the environment to be used, the appropriate tools and dependencies must be installed. The organization of this repository is divided by environments, where each environment has its own folder.


## Mininet

1) [Install Mininet](http://mininet.org/download/)
2) Clone this repo
3) Move to the mininet folder
4) Depending on the protocol you want to use, you must run the corresponding script. For example, if you want to use BGP, you must use the fattree_bgp.py file.

The script selected will be on charge of:
- Generate a mininet fat-tree topology
- For each mininet node, it will generates all the routing daemons configurations files in order to have full layer-3 connectivity
- Start the emulation and all the routing daemons
- Run the coded tests or display the Mininet CLI
- Stop the emulation

## Core

1) [Install Core Emulator](http://coreemu.github.io/core/install.html)
2) Clone this repo
3) Move to the core folder
4) Depending on the protocol you want to use, you must run the corresponding script. For example, if you want to use BGP, you must use the fat_tree_bgp.py file.

The script selected will be on charge of:
- Generate a mininet fat-tree topology
- For each mininet node, it will generates all the routing daemons configurations files in order to have full layer-3 connectivity
- Start the emulation and all the routing daemons
- Run the coded tests
- Stop the emulation


## GNS3

The gns3 folder has a single GNS3 project. 

1) [Install GNS3](https://www.gns3.com/software/download)
2) Clone this repo
3) Move to the gns3 folder
4) [Import the project](https://docs.gns3.com/docs/)
5) Start the emulation

## Kathara and Megalos

Refer Sibyl Framework at
https://gitlab.com/uniroma3/compunet/networks/sibyl-framework/sibyl


## How to reference this source code?
BibTeX

@Article{fi14010029,
AUTHOR = {Alberro, Leonardo and Castro, Alberto and Grampin, Eduardo},
TITLE = {Experimentation Environments for Data Center Routing Protocols: A Comprehensive Review},
JOURNAL = {Future Internet},
VOLUME = {14},
YEAR = {2022},
NUMBER = {1},
ARTICLE-NUMBER = {29},
URL = {https://www.mdpi.com/1999-5903/14/1/29},
ISSN = {1999-5903},
ABSTRACT = {The Internet architecture has been undergoing a significant refactoring, where the past preeminence of transit providers has been replaced by content providers, which have a ubiquitous presence throughout the world, seeking to improve the user experience, bringing content closer to its final recipients. This restructuring is materialized in the emergence of Massive Scale Data Centers (MSDC) worldwide, which allows the implementation of the Cloud Computing concept. MSDC usually deploy Fat-Tree topologies, with constant bisection bandwidth among servers and multi-path routing. To take full advantage of such characteristics, specific routing protocols are needed. Multi-path routing also calls for revision of transport protocols and forwarding policies, also affected by specific MSDC applications&rsquo; traffic characteristics. Experimenting over these infrastructures is prohibitively expensive, and therefore, scalable and realistic experimentation environments are needed to research and test solutions for MSDC. In this paper, we review several environments, both single-host and distributed, which permit analyzing the pros and cons of different solutions.},
DOI = {10.3390/fi14010029}
}

https://doi.org/10.3390/fi14010029


## Authors
Leonardo Alberro (lalberro@fing.edu.uy)

## Project status
Work in progress. More detailed instructions will be given soon.


***









