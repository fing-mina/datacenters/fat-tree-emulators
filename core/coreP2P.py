from core.emulator.coreemu import CoreEmu
from core.emulator.data import IpPrefixes, NodeOptions
from core.emulator.enumerations import EventTypes
from core.nodes.base import CoreNode, NodeBase
import os

try:
    # ip nerator for example
    ip_prefixes_l1 = IpPrefixes(ip4_prefix="10.0.0.0/24")
    ip_prefixes_l2 = IpPrefixes(ip4_prefix="10.0.1.0/24")

    # create emulator instance for creating sessions and utility methods
    coreemu = CoreEmu()
    session = coreemu.create_session()

    # must be in configuration state for nodes to start, when using "node_add" below
    session.set_state(EventTypes.CONFIGURATION_STATE)

    # create nodes
    options = NodeOptions(name="bla",icon="router",x=100, y=100, services=["FRRzebra","FRROSPFv2", "IPForward"])
    n1 = session.add_node(CoreNode, options=options)
    options = NodeOptions(x=300, y=100, services=["FRRzebra","FRROSPFv2", "IPForward"])
    n2 = session.add_node(CoreNode, options=options)
    options = NodeOptions(x=300, y=300, services=["FRRzebra","FRROSPFv2", "IPForward"])
    n3 = session.add_node(CoreNode, options=options)

    # link nodes together
    iface1 = ip_prefixes_l1.create_iface(n1)
    iface2 = ip_prefixes_l1.create_iface(n2)
    session.add_link(n1.id, n2.id, iface1, iface2)
    iface3 = ip_prefixes_l2.create_iface(n2)
    iface4 = ip_prefixes_l2.create_iface(n3)
    session.add_link(n2.id, n3.id, iface3, iface4)

    # start session
    session.instantiate()

    # do whatever you like here
    cmd1 = n1.termcmdstring()
    cmd2 = n2.termcmdstring()
    cmd3 = n3.termcmdstring()

    os.system("xterm -e "+ cmd1 + "&")
    os.system("xterm -e "+ cmd2 + "&")
    os.system("xterm -e "+ cmd3 + "&")

    input("press enter to shutdown")
finally:
        session.shutdown()