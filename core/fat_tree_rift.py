from core.emulator.coreemu import CoreEmu
from core.emulator.data import IpPrefixes, NodeOptions, InterfaceData
from core.emulator.enumerations import EventTypes
from core.nodes.base import CoreNode
import logging
import sys
import os

path_yaml =  "/home/lalberro/core/py-scripts/conf-rift/"
path_rift = "/home/lalberro/rift-python/"

logging.basicConfig(filename='logs/fattree_rift_core.log', level=logging.DEBUG)
logger = logging.getLogger(__name__)


class FatTree():

    CoreRouterList = []
    CoreRouterListASNs = []
    SpineRouterList = []
    SpineRouterListASNs = []
    LeafRouterList = []
    LeafRouterListASNs = []
    HostList = []


    def __init__( self, k):
        self.pod = k
        self.iCoreLayerRouter = int((k/2)**2)
        self.iSpineLayerRouter = int(k*k/2)
        self.iLeafLayerRouter = int(k*k/2)

        coreemu = CoreEmu()
        self.session = coreemu.create_session()
         # must be in configuration state for nodes to start, when using "node_add" below
        self.session.set_state(EventTypes.CONFIGURATION_STATE)
        
        if hosts < 0:
            self.density = int(k/2)
        else:
            self.density = hosts
            
        self.iHost = int(self.iLeafLayerRouter * self.density)
        
        showMessage("hosts: " + str(hosts))
        showMessage("pod: " + str(self.pod))
        showMessage("iCoreLayerRouter: " + str(self.iCoreLayerRouter))
        showMessage("iSpineLayerRouter: " + str(self.iSpineLayerRouter))
        showMessage("iLeafLayerRouter: " + str(self.iLeafLayerRouter))
        showMessage("density: " + str(self.density))
        showMessage("iHost: " + str(self.iHost))

        # create nodes
        services = ["IPForward"]
        self.createCoreLayerRouter(services)
        self.createSpineLayerRouter(services)
        self.createLeafLayerRouter(services)
        self.createHost()
        self.createLinks()
        self.createRIFTBaseFiles()
        self.configureRIFT()
        self.createRIFTFooterFiles()


    def createCoreLayerRouter(self, services):
        for i in range(1, self.iCoreLayerRouter+1):
            name = "Core-"+str(i)
            options = NodeOptions(name=name, x=200*i, y=100, services=services)
            node = self.session.add_node(CoreNode, options=options)
            self.CoreRouterList.append(node)
            print(f"created node: {node.name}")

    def createSpineLayerRouter(self, services):
        for i in range(1, self.iSpineLayerRouter+1):
            name = "Spine-"+str(int((i-1)/(k/2)))+"-"+str(int((i-1) % (k/2)))
            options = NodeOptions(name=name, x=100*i, y=400, services=services)
            node = self.session.add_node(CoreNode, options=options)
            self.SpineRouterList.append(node)
            print(f"created node: {node.name}")

    def createLeafLayerRouter(self, services):
        for i in range(1, self.iLeafLayerRouter+1):
            name = "Leaf-"+str(int((i-1)/(k/2)))+"-"+str(int((i-1) % (k/2)))
            options = NodeOptions(name=name, x=100*i, y=500, services=services)
            node = self.session.add_node(CoreNode, options=options)
            self.LeafRouterList.append(node)
            print(f"created node: {node.name}")

    def createHost(self):
        for i in range(1, self.iHost+1):
            name = "Host-"+str(int((i-1)/(k/2)))+"-"+str(int((i-1) % (k/2)))
            options = NodeOptions(name=name, x=10*i, y=600)
            node = self.session.add_node(CoreNode, options=options)
            self.HostList.append(node)
            print(f"created node: {node.name}")

    def createLinks(self):
        showMessage("Add link Core to Spine.")
        link_configured = 0
        octet = 0
        end = int(self.pod/2)
        for x in range(0, self.iSpineLayerRouter, end):
            core_interface_index = int(x / end)
            for i in range(0, end):
                for j in range(0, end):
                    if(link_configured>254):
                        octet += 1
                        link_configured = 0
                    spine_interface_index = j
                    n1 = self.CoreRouterList[i*end+j]
                    n2 = self.SpineRouterList[x+i]
                    eth1 = str(n1.name)+"-eth"+str(core_interface_index)
                    eth2 = str(n2.name)+"-eth"+str(spine_interface_index)
                    iface1 = InterfaceData(name = eth1, ip4="10."+str(octet)+"."+str(link_configured)+".1", ip4_mask="24")
                    iface2 = InterfaceData(name = eth2, ip4="10."+str(octet)+"."+str(link_configured)+".2", ip4_mask="24")
                    self.session.add_link(n1.id, n2.id, iface1, iface2)
                    link_configured = link_configured + 1

        showMessage("Add link Spine to Leaf.")
        for x in range(0, self.iSpineLayerRouter, end):
            for i in range(0, end):
                leaf_interface_index = i
                for j in range(0, end):
                    if(link_configured>254):
                        octet += 1
                        link_configured = 0
                    spine_interface_index = j + end
                    n1 = self.SpineRouterList[x+i]
                    n2 = self.LeafRouterList[x+j]
                    eth1 = str(n2.name)+"-eth"+str(leaf_interface_index)
                    eth2 = str(n1.name)+"-eth"+str(spine_interface_index)
                    iface1 = InterfaceData(name = eth2, ip4="10."+str(octet)+"."+str(link_configured)+".1", ip4_mask="24")
                    iface2 = InterfaceData(name = eth1, ip4="10."+str(octet)+"."+str(link_configured)+".2", ip4_mask="24")
                    self.session.add_link(n1.id, n2.id, iface1, iface2)
                    link_configured = link_configured + 1
        
        showMessage("Add link Leaf to Host.")
        link_configured = 0
        octet = 16
        for x in range(0, self.iLeafLayerRouter):
            for i in range(0, int(k/2)):
                if(link_configured>254):
                    octet += 1
                    link_configured = 0
                # print(f"created host: {str(x+i)}")
                # print(f"HostListLen: {len(self.HostList)}")
                leaf_interface_index = int(i + k/2)
                n1 = self.LeafRouterList[x]
                n2 = self.HostList[link_configured]
                eth1 = str(n1.name)+"-eth"+str(leaf_interface_index)
                eth2 = str(n2.name)+"-eth"+str(0)
                iface1 = InterfaceData(name = eth1, ip4="172."+str(octet)+"."+str(link_configured)+".1", ip4_mask="24")
                iface2 = InterfaceData(name = eth2, ip4="172."+str(octet)+"."+str(link_configured)+".2", ip4_mask="24")
                self.session.add_link(n1.id, n2.id, iface1, iface2)
                link_configured = link_configured + 1
                # host.cmd("ip route add default via 172.%s.%s.1" % (str(octet), str(link_configured)))

        

    def configureRIFT(self):
        showMessage("Configure RIFT Core to Spine.")
        end = int(self.pod/2)
        link_configured = 0
        octet = 0
        for x in range(0, self.iSpineLayerRouter, end):
            core_interface_index = int(x / end)
            for i in range(0, end):
                spine_router = self.SpineRouterList[x+i]
                spine_rift_file = open(path_yaml+ spine_router.name + ".yaml","a")
                for j in range(0, end):
                    if(link_configured>254):
                        octet += 1
                        link_configured = 0
                    spine_interface_index = j
                    core_router = self.CoreRouterList[i*end+j]
                    core_rift_file = open(path_yaml+ core_router.name + ".yaml","a")
                    try:
                        intf_local = "%s-eth%s " % (core_router.name, core_interface_index)
                        intf_peer = "%s-eth%s" % (spine_router.name, spine_interface_index)

                        core_rift_file.write("          - name: "+ intf_local +"\n")
                        description = "{} -> {}".format(intf_local, intf_peer)
                        core_rift_file.write("            # " + description +"\n")

                        intf_peer = "%s-eth%s" % (spine_router.name, spine_interface_index)
                        spine_rift_file.write("          - name: "+ intf_peer +"\n")
                        description = "{} -> {}".format(intf_peer, intf_local)
                        spine_rift_file.write("            # " + description +"\n")
                    finally:
                        core_rift_file.close()
                    link_configured = link_configured + 1	
                spine_rift_file.close()
                
        showMessage("Configure RIFT Spine to Leaf.")

        for x in range(0, self.iSpineLayerRouter, end):
            for i in range(0, end):
                leaf_interface_index = i
                spine_router = self.SpineRouterList[x+i]
                spine_rift_file = open(path_yaml + spine_router.name + ".yaml","a")
                for j in range(0, end):
                    if(link_configured>254):
                        octet += 1
                        link_configured = 0
                    spine_interface_index = j + end
                    leaf_router = self.LeafRouterList[x+j]
                    leaf_rift_file = open(path_yaml + leaf_router.name + ".yaml","a")
                    try:
                        intf_local = "%s-eth%s " % (leaf_router.name, leaf_interface_index)
                        intf_peer = "%s-eth%s" % (spine_router.name, spine_interface_index)

                        leaf_rift_file.write("          - name: "+ intf_local +"\n")
                        description = "{} -> {}".format(intf_local, intf_peer)
                        leaf_rift_file.write("            # " + description +"\n")

                        spine_rift_file.write("          - name: "+ intf_peer +"\n")
                        description = "{} -> {}".format(intf_peer, intf_local)
                        spine_rift_file.write("            # " + description +"\n")
                    finally:
                        leaf_rift_file.close()
        
                    link_configured = link_configured + 1
                spine_rift_file.close()
                

        link_configured = 0
        octet = 16
        for x in range(0, self.iLeafLayerRouter):
            leaf_router = self.LeafRouterList[x]
            # leaf_rift_file = open(path_yaml + leaf_router.name + ".yaml","a")
            for i in range(0, self.density):
                if(link_configured>254):
                    octet += 1
                    link_configured = 0
                intf_local = "%s-eth%s " % (leaf_router.name, int(i + k/2))
                leaf_rift_file = open(path_yaml + leaf_router.name + ".yaml","a")
                try:
                    intf_local = "%s-eth%s " % (leaf_router.name, int(i + k/2))
                    leaf_rift_file.write("          - name: "+ intf_local +"\n")
                finally:
                    leaf_rift_file.close()
                                
                link_configured = link_configured + 1
            
            leaf_rift_file.close()

    def createRIFTBaseFiles(self):
        end = int(k/2)
        for x in range(0, self.iCoreLayerRouter):
            router = self.CoreRouterList[x]
            fs = open(path_yaml + router.name + ".yaml","w+")
            try:
                fs.write("shards:\n")
                fs.write("  - id: 0\n")
                fs.write("    nodes:\n")
                fs.write("      - name: " + router.name +  "\n")
                fs.write("        level: 2\n")
                fs.write("        systemid: " + str(x+1) + "\n")
                fs.write("        interfaces:\n")		
            finally:
                fs.close()

        for x in range(0, self.iSpineLayerRouter):
            router = self.SpineRouterList[x]
            fs = open(path_yaml+ router.name + ".yaml","w+")
            try:
                fs.write("shards:\n")
                fs.write("  - id: 0\n")
                fs.write("    nodes:\n")
                fs.write("      - name: " + router.name +  "\n")
                fs.write("        level: 1\n")
                fs.write("        systemid: 10" + str(x+1) + "\n")
                fs.write("        interfaces:\n")
            finally:
                fs.close()

        for x in range(0, self.iLeafLayerRouter, end):
            for i in range(0, end):
                router = self.LeafRouterList[x + i]
                fs = open(path_yaml+ router.name + ".yaml","w+")
                try:
                    fs.write("shards:\n")
                    fs.write("  - id: 0\n")
                    fs.write("    nodes:\n")
                    fs.write("      - name: " + router.name +  "\n")
                    fs.write("        level: 0\n")
                    fs.write("        systemid: 100" + str(x+i) + "\n")
                    fs.write("        interfaces:\n")
                finally:
                    fs.close()

    def createRIFTFooterFiles(self):
        octet = 16
        link_configured = 0
        metric = 1
        mask = 24
        for x in range(0, self.iLeafLayerRouter):
            router = self.LeafRouterList[x]
            fs = open(path_yaml+ router.name + ".yaml","a")
            try:
                fs.write("        v4prefixes:\n")
            finally:
                fs.close()
            for i in range(0, self.density):
                if(link_configured>254):
                    octet += 1
                    link_configured = 0
                address1 = "172."+str(octet)+"."+ str(int(self.density * x + i)) +".0"
                fs = open(path_yaml+ router.name + ".yaml","a")
                try:
                    fs.write("          - address: " + address1 +"\n")
                    fs.write("            mask: " + str(mask) +"\n")
                    fs.write("            metric: " + str(metric) + "\n")
                finally:
                    fs.close()
                link_configured+=1

def startDaemons(topo):
    for x in range(0, topo.iCoreLayerRouter):
        router = topo.CoreRouterList[x]
        router.cmd("%senv/bin/python3 %srift  --ipv6-multicast-loopback-disable --telnet-port-file /tmp/rift-python-telnet-port-%s %s%s.yaml >/dev/null 2>&1 &" % (path_rift, path_rift, router.name, path_yaml, router.name), shell=True)
    
    showMessage("End Cores")

    for x in range(0, topo.iSpineLayerRouter):
        router = topo.SpineRouterList[x]
        router.cmd("%senv/bin/python3 %srift  --ipv6-multicast-loopback-disable --telnet-port-file /tmp/rift-python-telnet-port-%s %s%s.yaml >/dev/null 2>&1 &" % (path_rift, path_rift, router.name, path_yaml, router.name), shell=True)

    showMessage("End Spines")

    for x in range(0, topo.iLeafLayerRouter):
        router = topo.LeafRouterList[x]
        router.cmd("%senv/bin/python3 %srift  --ipv6-multicast-loopback-disable --telnet-port-file /tmp/rift-python-telnet-port-%s %s%s.yaml >/dev/null 2>&1 &" % (path_rift, path_rift, router.name, path_yaml, router.name), shell=True)

    showMessage("End Leaves")


def showMessage(message):
	logger.debug(message)
	sys.stdout.write(message + "\n")
	sys.stdout.flush

def dumpTables(topo):
    showMessage("Dumping forwarding tables")
    for x in range(0, topo.iLeafLayerRouter):
        router = topo.LeafRouterList[x]
        router.cmd(f"ip route > {path}tables/table_{router.name}.txt", shell=True)
    for x in range(0, topo.iSpineLayerRouter):
        router = topo.SpineRouterList[x]
        router.cmd(f"ip route > {path}tables/table_{router.name}.txt", shell=True)
    for x in range(0, topo.iCoreLayerRouter):
        router = topo.CoreRouterList[x]
        router.cmd(f"ip route > {path}tables/table_{router.name}.txt", shell=True)



if __name__ == "__main__":
    try: 
        if len(sys.argv) < 2:
            print ('fat_tree_rift.py k <hosts per leaf>')
            sys.exit(2)
        
        k = int(sys.argv[1])
        hosts = -1
        
        if k & 1:
            print ('k must be even')
            sys.exit(2)

        if len(sys.argv) > 2:
            hosts = int(sys.argv[2])

        if hosts > k/2:
            print ('hosts must be <= k/2')
            sys.exit(2)

        os.system("rm -f /tmp/*.pid logs/*.stdout")
        os.system("killall -9 rift > /dev/null 2>&1")
        os.system("rm -f *.log")
        os.system("rm -f /tmp/rift*")
        os.system("rm -f "+ path_yaml+"*.yaml")

        if not os.path.exists('conf'):
            os.makedirs('conf')
        else:
            os.system("rm -f conf/*")
        
        if not os.path.exists('tables'):
            os.makedirs('tables')
        else:
            os.system("rm -f tables/*")

        if not os.path.exists('logs'):
            os.makedirs('logs')

        if not os.path.exists('pcaps'):
            os.makedirs('pcaps')
        else:
            os.system("rm -f pcaps/*")
            
        path = os.popen('pwd').read().strip()
        path+="/"
        print("PATH for conf/ and logs/: "+str(path))
    
        topo=FatTree(k)
        topo.session.instantiate()
        startDaemons(topo)
        cmd1 = topo.LeafRouterList[1].termcmdstring()
        cmd2 = topo.LeafRouterList[topo.iLeafLayerRouter-1].termcmdstring()

        os.system("xterm -e "+ cmd1 + "&")
        os.system("xterm -e "+ cmd2 + "&")

        input("press enter to shutdown")

    finally:
        topo.session.shutdown()
        os.system("killall -9 rift > /dev/null 2>&1")