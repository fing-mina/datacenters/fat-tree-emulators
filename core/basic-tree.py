from core.emulator.coreemu import CoreEmu
from core.emulator.data import IpPrefixes, NodeOptions
from core.emulator.enumerations import EventTypes
from core.nodes.base import CoreNode
import logging
import sys
import os

logging.basicConfig(filename='logs/fattree_bgp_core.log', level=logging.DEBUG)
logger = logging.getLogger(__name__)


class FatTree():

    CoreRouterList = []
    CoreRouterListASNs = []
    SpineRouterList = []
    SpineRouterListASNs = []
    LeafRouterList = []
    LeafRouterListASNs = []
    HostList = []


    def __init__( self, k):
        self.pod = k
        self.iCoreLayerRouter = int((k/2)**2)
        self.iSpineLayerRouter = int(k*k/2)
        self.iLeafLayerRouter = int(k*k/2)

        coreemu = CoreEmu()
        self.session = coreemu.create_session()
         # must be in configuration state for nodes to start, when using "node_add" below
        self.session.set_state(EventTypes.CONFIGURATION_STATE)
        
        if hosts < 0:
            self.density = int(k/2)
        else:
            self.density = hosts
            
        iHost = int(self.iLeafLayerRouter * self.density)
        
        showMessage("hosts: " + str(hosts))
        showMessage("pod: " + str(self.pod))
        showMessage("iCoreLayerRouter: " + str(self.iCoreLayerRouter))
        showMessage("iSpineLayerRouter: " + str(self.iSpineLayerRouter))
        showMessage("iLeafLayerRouter: " + str(self.iLeafLayerRouter))
        showMessage("density: " + str(self.density))
        showMessage("iHost: " + str(iHost))

        # create nodes
        services = ["FRRzebra","FRROSPFv2", "IPForward"]
        self.createCoreLayerRouter(services)
        self.createSpineLayerRouter(services)
        self.createLeafLayerRouter(services)
        # self.createHost()
        self.createLinks()
        print(f"id node: {self.LeafRouterList[1].id}")
        print(self.LeafRouterList[1].get_ifaces()[1].get_ip4())


    def createCoreLayerRouter(self, services):
        for i in range(1, self.iCoreLayerRouter+1):
            name = "Core-"+str(i)
            options = NodeOptions(name=name, x=200*i, y=100, services=services)
            node = self.session.add_node(CoreNode, options=options)
            self.CoreRouterList.append(node)
            print(f"created node: {node.name}")

    def createSpineLayerRouter(self, services):
        for i in range(1, self.iSpineLayerRouter+1):
            name = "Spine-"+str(int((i-1)/(k/2)))+"-"+str(int((i-1) % (k/2)))
            options = NodeOptions(name=name, x=100*i, y=400, services=services)
            node = self.session.add_node(CoreNode, options=options)
            self.SpineRouterList.append(node)
            print(f"created node: {node.name}")

    def createLeafLayerRouter(self, services):
        for i in range(1, self.iLeafLayerRouter+1):
            name = "Leaf-"+str(int((i-1)/(k/2)))+"-"+str(int((i-1) % (k/2)))
            options = NodeOptions(name=name, x=100*i, y=500, services=services)
            node = self.session.add_node(CoreNode, options=options)
            self.LeafRouterList.append(node)
            print(f"created node: {node.name}")

    # def createHost(session):
    #     print('TODO')

    def createLinks(self):
        showMessage("Add link Core to Spine.")
        link_configured = 0
        octet = 0
        end = int(self.pod/2)
        for x in range(0, self.iSpineLayerRouter, end):
            for i in range(0, end):
                for j in range(0, end):
                    if(link_configured>254):
                        octet += 1
                        link_configured = 0
                    ip_prefixes_core_spine = IpPrefixes(ip4_prefix="10."+str(octet)+"."+str(link_configured)+".0/24")
                    n1 = self.CoreRouterList[i*end+j]
                    n2 = self.SpineRouterList[x+i]
                    iface1 = ip_prefixes_core_spine.create_iface(n1)
                    iface2 = ip_prefixes_core_spine.create_iface(n2)
                    # showMessage("link1: "+ ip_prefixes_core_spine.ip4_address(n1.id))
                    # showMessage("link2: "+ ip_prefixes_core_spine.ip4_address(n2.id))
                    self.session.add_link(n1.id, n2.id, iface1, iface2)
                    link_configured = link_configured + 1

        showMessage("Add link Spine to Leaf.")
        for x in range(0, self.iSpineLayerRouter, end):
            for i in range(0, end):
                for j in range(0, end):
                    if(link_configured>254):
                        octet += 1
                        link_configured = 0
                    ip_prefixes_spines_leaf = IpPrefixes(ip4_prefix="10."+str(octet)+"."+str(link_configured)+".0/24")
                    n1 = self.SpineRouterList[x+i]
                    n2 = self.LeafRouterList[x+j]
                    iface1 = ip_prefixes_spines_leaf.create_iface(n1)
                    iface2 = ip_prefixes_spines_leaf.create_iface(n2)
                    self.session.add_link(n1.id, n2.id, iface1, iface2)
                    link_configured = link_configured + 1
        
        # showMessage("Add link Leaf to Host.")
        # for x in range(0, self.iLeafLayerRouter):
        #     ip_prefixes_leaf_host = IpPrefixes(ip4_prefix="172.31."+str(i)+".0/24")
        #     for i in range(0, self.density):
        #         n1 = self.LeafRouterList[x]
        #         n2 = self.HostList[self.density * x + i]
        #         iface1 = ip_prefixes_leaf_host.create_iface(n1)
        #         iface2 = ip_prefixes_leaf_host.create_iface(n2)
        #         self.session.add_link(n1.id, n2.id, iface1, iface2)

        

def showMessage(message):
	logger.debug(message)
	sys.stdout.write(message + "\n")
	sys.stdout.flush



if __name__ == "__main__":
    try: 
        if len(sys.argv) < 2:
            print ('fattree_isis.py k <hosts per leaf>')
            sys.exit(2)
        
        k = int(sys.argv[1])
        hosts = -1
        
        if k & 1:
            print ('k must be even')
            sys.exit(2)

        if len(sys.argv) > 2:
            hosts = int(sys.argv[2])

        if hosts > k/2:
            print ('hosts must be <= k/2')
            sys.exit(2)
    
        topo=FatTree(k)
        topo.session.instantiate()

        cmd1 = topo.LeafRouterList[1].termcmdstring()
        cmd2 = topo.LeafRouterList[topo.iLeafLayerRouter-1].termcmdstring()

        os.system("xterm -e "+ cmd1 + "&")
        os.system("xterm -e "+ cmd2 + "&")

        input("press enter to shutdown")

    finally:
        topo.session.shutdown()