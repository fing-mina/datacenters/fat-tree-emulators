# required imports
from core.emulator.coreemu import CoreEmu
from core.emulator.data import IpPrefixes, InterfaceData
from core.emulator.enumerations import EventTypes
from core.nodes.base import CoreNode
import sys
import os

try:

    # create emulator instance for creating sessions and utility methods
    coreemu = CoreEmu()
    session = coreemu.create_session()

    # must be in configuration state for nodes to start, when using "node_add" below
    session.set_state(EventTypes.CONFIGURATION_STATE)

    # create nodes
    for i in range(1, 4):
        node = session.add_node(CoreNode)
        print(f"created node: {node.id}")

    n1 = session.get_node(1, CoreNode)
    n2 = session.get_node(2, CoreNode)
    prefix = IpPrefixes(ip4_prefix = "192.168.1.1/24")
    prefix2 = IpPrefixes(ip4_prefix = "192.168.1.2/24")
    # iface1 = IpPrefixes.gen_iface(prefix, node_id = n1.id)
    # iface2 = IpPrefixes.gen_iface(prefix2, node_id = n2.id)
    iface1 = InterfaceData(ip4="192.168.1.1", ip4_mask="24")
    iface2 = InterfaceData(ip4="192.168.1.2", ip4_mask="24")
    iface3 = InterfaceData(ip4="192.168.2.1", ip4_mask="24")
    iface4 = InterfaceData(ip4="192.168.2.2", ip4_mask="24")
    print("iface1: "+ iface1.get_ips()[0])
    print("iface2: "+ iface2.get_ips()[0])
    session.add_link(n1.id, n2.id, iface1, iface2)
    session.add_link(n1.id, n2.id, iface3, iface4)
    n1.cmd("ifconfig > /tmp/out.log", shell=True) 

    

    # start session
    session.instantiate()

    cmd1 = n1.termcmdstring()
    cmd2 = n2.termcmdstring()

    os.system("xterm -e "+ cmd1 + "&")
    os.system("xterm -e "+ cmd2 + "&")

    # do whatever you like here
    input("press enter to shutdown")
finally:
    # stop session
    session.shutdown()