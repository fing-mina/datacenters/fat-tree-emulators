from core.emulator.coreemu import CoreEmu
from core.emulator.data import IpPrefixes, NodeOptions, InterfaceData
from core.emulator.enumerations import EventTypes
from core.nodes.base import CoreNode
import logging
import sys
import os
import time

logging.basicConfig(filename='logs/fattree_bgp_core.log', level=logging.DEBUG)
logger = logging.getLogger(__name__)


class FatTree():

    CoreRouterList = []
    CoreRouterListASNs = []
    SpineRouterList = []
    SpineRouterListASNs = []
    LeafRouterList = []
    LeafRouterListASNs = []
    HostList = []


    def __init__( self, k):
        self.pod = k
        self.iCoreLayerRouter = int((k/2)**2)
        self.iSpineLayerRouter = int(k*k/2)
        self.iLeafLayerRouter = int(k*k/2)

        coreemu = CoreEmu()
        self.session = coreemu.create_session()
         # must be in configuration state for nodes to start, when using "node_add" below
        self.session.set_state(EventTypes.CONFIGURATION_STATE)
        
        if hosts < 0:
            self.density = int(k/2)
        else:
            self.density = hosts
            
        iHost = int(self.iLeafLayerRouter * self.density)
        
        showMessage("hosts: " + str(hosts))
        showMessage("pod: " + str(self.pod))
        showMessage("iCoreLayerRouter: " + str(self.iCoreLayerRouter))
        showMessage("iSpineLayerRouter: " + str(self.iSpineLayerRouter))
        showMessage("iLeafLayerRouter: " + str(self.iLeafLayerRouter))
        showMessage("density: " + str(self.density))
        showMessage("iHost: " + str(iHost))

        # create nodes
        services = ["IPForward"]
        self.createCoreLayerRouter(services)
        self.createSpineLayerRouter(services)
        self.createLeafLayerRouter(services)
        # self.createHost()
        self.createLinks()
        self.createBGPZebraHeaderFiles()
        self.configureBGP()
        self.createBGPZebraFooterFiles()


    def createCoreLayerRouter(self, services):
        for i in range(1, self.iCoreLayerRouter+1):
            name = "Core-"+str(i)
            options = NodeOptions(name=name, x=200*i, y=100, services=services)
            node = self.session.add_node(CoreNode, options=options)
            self.CoreRouterList.append(node)
            print(f"created node: {node.name}")

    def createSpineLayerRouter(self, services):
        for i in range(1, self.iSpineLayerRouter+1):
            name = "Spine-"+str(int((i-1)/(k/2)))+"-"+str(int((i-1) % (k/2)))
            options = NodeOptions(name=name, x=100*i, y=400, services=services)
            node = self.session.add_node(CoreNode, options=options)
            self.SpineRouterList.append(node)
            print(f"created node: {node.name}")

    def createLeafLayerRouter(self, services):
        for i in range(1, self.iLeafLayerRouter+1):
            name = "Leaf-"+str(int((i-1)/(k/2)))+"-"+str(int((i-1) % (k/2)))
            options = NodeOptions(name=name, x=100*i, y=500, services=services)
            node = self.session.add_node(CoreNode, options=options)
            self.LeafRouterList.append(node)
            print(f"created node: {node.name}")

    # def createHost(session):
    #     print('TODO')

    def createLinks(self):
        showMessage("Add link Core to Spine.")
        link_configured = 0
        octet = 0
        end = int(self.pod/2)
        for x in range(0, self.iSpineLayerRouter, end):
            core_interface_index = int(x / end)
            for i in range(0, end):
                for j in range(0, end):
                    if(link_configured>254):
                        octet += 1
                        link_configured = 0
                    spine_interface_index = j
                    # ip_prefixes_core_spine = IpPrefixes(ip4_prefix="10."+str(octet)+"."+str(link_configured)+".0/24")
                    eth1 = "eth-"+str(core_interface_index)
                    eth2 = "eth-"+str(spine_interface_index)
                    iface1 = InterfaceData(name = eth1, ip4="10."+str(octet)+"."+str(link_configured)+".1", ip4_mask="24")
                    iface2 = InterfaceData(name = eth2, ip4="10."+str(octet)+"."+str(link_configured)+".2", ip4_mask="24")
                    n1 = self.CoreRouterList[i*end+j]
                    n2 = self.SpineRouterList[x+i]
                    # print("iface1: "+ iface1.get_ips()[0])
                    # print("iface2: "+ iface2.get_ips()[0])
                    # iface1 = ip_prefixes_core_spine.create_iface(n1)
                    # iface2 = ip_prefixes_core_spine.create_iface(n2)
                    # showMessage("link1: "+ ip_prefixes_core_spine.ip4_address(n1.id))
                    # showMessage("link2: "+ ip_prefixes_core_spine.ip4_address(n2.id))
                    self.session.add_link(n1.id, n2.id, iface1, iface2)
                    link_configured = link_configured + 1

        showMessage("Add link Spine to Leaf.")
        for x in range(0, self.iSpineLayerRouter, end):
            for i in range(0, end):
                leaf_interface_index = i
                for j in range(0, end):
                    if(link_configured>254):
                        octet += 1
                        link_configured = 0
                    spine_interface_index = j + end
                    # ip_prefixes_spines_leaf = IpPrefixes(ip4_prefix="10."+str(octet)+"."+str(link_configured)+".0/24")
                    eth1 = "eth-"+str(leaf_interface_index)
                    eth2 = "eth-"+str(spine_interface_index)
                    iface1 = InterfaceData(name = eth2, ip4="10."+str(octet)+"."+str(link_configured)+".1", ip4_mask="24")
                    iface2 = InterfaceData(name = eth1, ip4="10."+str(octet)+"."+str(link_configured)+".2", ip4_mask="24")
                    n1 = self.SpineRouterList[x+i]
                    n2 = self.LeafRouterList[x+j]
                    # iface1 = ip_prefixes_spines_leaf.create_iface(n1)
                    # iface2 = ip_prefixes_spines_leaf.create_iface(n2)
                    self.session.add_link(n1.id, n2.id, iface1, iface2)
                    link_configured = link_configured + 1

    
    def configureBGP(self):
        showMessage("Configure BGP Core to Spine.")
        link_configured = 0
        octet = 0
        end = int(self.pod/2)
        for x in range(0, self.iSpineLayerRouter, end):
            core_interface_index = int(x / end)
            for i in range(0, end):
                spine_router = self.SpineRouterList[x+i]
                spine_bgpd_file = open("conf/bgpd_" + spine_router.name + ".conf","a")
                spine_zebra_file = open("conf/zebra_" + spine_router.name + ".conf","a")
                for j in range(0, end):
                    if(link_configured>254):
                        octet += 1
                        link_configured = 0
                    spine_interface_index = j
                    core_router = self.CoreRouterList[i*end+j]
                    spine_bgpd_file.write(" neighbor 10.%s.%s.1 remote-as %s\n" % (octet, link_configured, self.CoreRouterListASNs[i*end+j]))
                    spine_bgpd_file.write(" neighbor 10.%s.%s.1 timers 5 5\n" % (octet, link_configured))
                    core_bgpd_file = open("conf/bgpd_" + core_router.name + ".conf","a")
                    core_bgpd_file.write(" neighbor 10.%s.%s.2 remote-as %s\n" % (octet, link_configured, self.SpineRouterListASNs[x+i]))
                    core_bgpd_file.write(" neighbor 10.%s.%s.2 timers 5 5\n" % (octet, link_configured))
                    core_bgpd_file.close()
                    core_zebra_file = open("conf/zebra_" + core_router.name + ".conf","a")
                    core_zebra_file.write("interface eth%s\n" % (core_interface_index))
                    core_zebra_file.write(" ip address 10.%s.%s.1/24\n" % (octet, link_configured))
                    core_zebra_file.close()
                    spine_zebra_file.write("interface eth%s\n" % (spine_interface_index))
                    spine_zebra_file.write(" ip address 10.%s.%s.2/24\n" % (octet, link_configured))
                    link_configured = link_configured + 1
                spine_zebra_file.close()
                spine_bgpd_file.close()

        showMessage("Configure BGP Spine to Leaf.")
        for x in range(0, self.iSpineLayerRouter, end):
            for i in range(0, end):
                leaf_interface_index = i
                spine_router = self.SpineRouterList[x+i]
                spine_zebra_file = open("conf/zebra_" + spine_router.name + ".conf","a")
                spine_bgpd_file = open("conf/bgpd_" + spine_router.name + ".conf","a")
                for j in range(0, end):
                    if(link_configured>254):
                        octet += 1
                        link_configured = 0
                    spine_interface_index = j + end
                    leaf_router = self.LeafRouterList[x+j]
                    spine_bgpd_file.write(" neighbor 10.%s.%s.2 remote-as %s\n" % (octet, link_configured, self.LeafRouterListASNs[x+j]))
                    spine_bgpd_file.write(" neighbor 10.%s.%s.2 timers 5 5\n" % (octet, link_configured))
                    leaf_bgpd_file = open("conf/bgpd_" + leaf_router.name + ".conf","a")
                    leaf_bgpd_file.write(" neighbor 10.%s.%s.1 remote-as %s\n" % (octet, link_configured, self.SpineRouterListASNs[x+i]))
                    leaf_bgpd_file.write(" neighbor 10.%s.%s.1 timers 5 5\n" % (octet, link_configured))
                    leaf_bgpd_file.close()
                    leaf_zebra_file = open("conf/zebra_" + leaf_router.name + ".conf","a")
                    leaf_zebra_file.write("interface eth%s\n" % (leaf_interface_index))
                    leaf_zebra_file.write(" ip address 10.%s.%s.2/24\n" % (octet, link_configured))
                    leaf_zebra_file.close()
                    spine_zebra_file.write("interface eth%s\n" % (spine_interface_index))
                    spine_zebra_file.write(" ip address 10.%s.%s.1/24\n" % (octet, link_configured))
                    link_configured = link_configured + 1
                spine_zebra_file.close()
                spine_bgpd_file.close()
        
        # showMessage("Add link Leaf to Host.")
        # for x in range(0, self.iLeafLayerRouter):
        #     ip_prefixes_leaf_host = IpPrefixes(ip4_prefix="172.31."+str(i)+".0/24")
        #     for i in range(0, self.density):
        #         n1 = self.LeafRouterList[x]
        #         n2 = self.HostList[self.density * x + i]
        #         iface1 = ip_prefixes_leaf_host.create_iface(n1)
        #         iface2 = ip_prefixes_leaf_host.create_iface(n2)
        #         self.session.add_link(n1.id, n2.id, iface1, iface2)

    def createBGPZebraHeaderFiles(self):
        end = int(k/2)
        asn_nr = 65534
        for x in range(0, self.iCoreLayerRouter):
            self.CoreRouterListASNs.append(str(asn_nr))
            router = self.CoreRouterList[x]
            file = open("conf/bgpd_" + router.name + ".conf","w")
            file.write("hostname " + router.name + "\n")
            file.write("password en\n")
            file.write("enable password en\n")
            file.write("log file /tmp/" + router.name + "-bgpd.log\n")
            file.write("router bgp " + str(asn_nr) + "\n")
            file.write(" bgp router-id 10.0.254.1\n")
            file.write(" bgp bestpath as-path multipath-relax\n")
            file.close()
            file = open("conf/zebra_" + router.name + ".conf","w")
            file.write("hostname " + router.name + "\n")
            file.write("password en\n")
            file.write("enable password en\n")
            file.write("log file /tmp/" + router.name + "-zebra.log\n")
            file.close()

        for x in range(0, self.iSpineLayerRouter):
            pod_nr = int(x / end)
            asn_nr = 65000 + (pod_nr * k)
            self.SpineRouterListASNs.append(str(asn_nr))
            router = self.SpineRouterList[x]
            file = open("conf/bgpd_" + router.name + ".conf","w")
            file.write("hostname " + router.name + "\n")
            file.write("password en\n")
            file.write("enable password en\n")
            file.write("log file /tmp/" + router.name + "-bgpd.log\n")
            file.write("router bgp " + str(asn_nr) + "\n")
            file.write(" bgp router-id 10.0.254.2\n")
            file.write(" bgp bestpath as-path multipath-relax\n")
            file.close()
            file = open("conf/zebra_" + router.name + ".conf","w")
            file.write("hostname " + router.name + "\n")
            file.write("password en\n")
            file.write("enable password en\n")
            file.write("log file /tmp/" + router.name + "-zebra.log\n")
            file.close()

        for x in range(0, self.iLeafLayerRouter, end):
            pod_nr = int(x / end)
            for i in range(0, end):
                asn_nr = 65000 + (pod_nr * k) + i + 1
                self.LeafRouterListASNs.append(str(asn_nr))
                router = self.LeafRouterList[x + i]
                file = open("conf/bgpd_" + router.name + ".conf","w")
                file.write("hostname " + router.name + "\n")
                file.write("password en\n")
                file.write("enable password en\n")
                file.write("log file /tmp/" + router.name + "-bgpd.log\n")
                file.write("router bgp " + str(asn_nr) + "\n")
                file.write(" bgp router-id 10.1.254.2\n")
                file.write(" bgp bestpath as-path multipath-relax\n")
                file.close()
                file = open("conf/zebra_" + router.name + ".conf","w")
                file.write("hostname " + router.name + "\n")
                file.write("password en\n")
                file.write("enable password en\n")
                file.write("log file /tmp/" + router.name + "-zebra.log\n")
                file.close()

    def createBGPZebraFooterFiles(self):
        for x in range(0, self.iCoreLayerRouter):
            router = self.CoreRouterList[x]
            file = open("conf/bgpd_" + router.name + ".conf","a")
            file.write(" redistribute connected\n")
            file.write("log stdout\n")
            file.close()

        for x in range(0, self.iSpineLayerRouter):
            router = self.SpineRouterList[x]
            file = open("conf/bgpd_" + router.name + ".conf","a")
            file.write(" redistribute connected\n")
            file.write("log stdout\n")
            file.close()

        for x in range(0, self.iLeafLayerRouter):
            router = self.LeafRouterList[x]
            file = open("conf/bgpd_" + router.name + ".conf","a")
            file.write(" redistribute connected\n")
            file.write("log stdout\n")
            file.close()

def startDaemons(topo):
    for x in range(0, topo.iCoreLayerRouter):
        router = topo.CoreRouterList[x]
        router.cmd("/usr/lib/frr/zebra -f %sconf/zebra_%s.conf -d -i /tmp/zebra_%s.pid > %slogs/%s_zebra.stdout 2>&1" % (path, router.name, router.name, path, router.name))
        router.cmd("/usr/lib/frr/bgpd --ecmp 64 -f %sconf/bgpd_%s.conf -d -i /tmp/bgpd_%s.pid --log file:%slogs/%s_bgpd.stdout" % (path, router.name, router.name, path, router.name), shell=True)

    showMessage("End Cores")

    for x in range(0, topo.iSpineLayerRouter):
        router = topo.SpineRouterList[x]
        router.cmd("/usr/lib/frr/zebra -f %sconf/zebra_%s.conf -d -i /tmp/zebra_%s.pid > %slogs/%s_zebra.stdout 2>&1" % (path, router.name, router.name, path, router.name))
        router.cmd("/usr/lib/frr/bgpd --ecmp 64 -f %sconf/bgpd_%s.conf -d -i /tmp/bgpd_%s.pid --log file:%slogs/%s_bgpd.stdout" % (path, router.name, router.name, path, router.name), shell=True)

    showMessage("End Spines")

    for x in range(0, topo.iLeafLayerRouter):
        router = topo.LeafRouterList[x]
        router.cmd("/usr/lib/frr/zebra -f %sconf/zebra_%s.conf -d -i /tmp/zebra_%s.pid > %slogs/%s_zebra.stdout 2>&1" % (path, router.name, router.name, path, router.name))
        router.cmd("/usr/lib/frr/bgpd --ecmp 64 -f %sconf/bgpd_%s.conf -d -i /tmp/bgpd_%s.pid --log file:%slogs/%s_bgpd.stdout" % (path, router.name, router.name, path, router.name), shell=True)

    showMessage("End Leaves")


def showMessage(message):
	logger.debug(message)
	sys.stdout.write(message + "\n")
	sys.stdout.flush

def dumpTables(topo):
    showMessage("Dumping forwarding tables")
    for x in range(0, topo.iLeafLayerRouter):
        router = topo.LeafRouterList[x]
        router.cmd(f"ip route > {path}tables/table_{router.name}.txt", shell=True)
    for x in range(0, topo.iSpineLayerRouter):
        router = topo.SpineRouterList[x]
        router.cmd(f"ip route > {path}tables/table_{router.name}.txt", shell=True)
    for x in range(0, topo.iCoreLayerRouter):
        router = topo.CoreRouterList[x]
        router.cmd(f"ip route > {path}tables/table_{router.name}.txt", shell=True)


if __name__ == "__main__":
    try: 
        if len(sys.argv) < 2:
            print ('fattree_isis.py k <hosts per leaf>')
            sys.exit(2)
        
        k = int(sys.argv[1])
        hosts = -1
        
        if k & 1:
            print ('k must be even')
            sys.exit(2)

        if len(sys.argv) > 2:
            hosts = int(sys.argv[2])

        if hosts > k/2:
            print ('hosts must be <= k/2')
            sys.exit(2)

        os.system("rm -f /tmp/*.pid logs/*.stdout")
        os.system("killall -9 bgpd zebra > /dev/null 2>&1")
        if not os.path.exists('conf'):
            os.makedirs('conf')
        else:
            os.system("rm -f conf/*")
        
        if not os.path.exists('tables'):
            os.makedirs('tables')
        else:
            os.system("rm -f tables/*")

        if not os.path.exists('logs'):
            os.makedirs('logs')

        if not os.path.exists('pcaps'):
            os.makedirs('pcaps')
        else:
            os.system("rm -f pcaps/*")
            
        path = os.popen('pwd').read().strip()
        path+="/"
        print("PATH for conf/ and logs/: "+str(path))
    
        topo=FatTree(k)
        topo.session.instantiate()
        startDaemons(topo)
        time.sleep(60)
        dumpTables(topo)
        # cmd1 = topo.LeafRouterList[1].termcmdstring()
        # cmd2 = topo.LeafRouterList[topo.iLeafLayerRouter-1].termcmdstring()

        # os.system("xterm -e "+ cmd1 + "&")
        # os.system("xterm -e "+ cmd2 + "&")

        input("press enter to shutdown")

    finally:
        topo.session.shutdown()
        os.system("killall -9 bgpd zebra > /dev/null 2>&1")