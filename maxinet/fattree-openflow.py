#!/usr/bin/python2


import time

from MaxiNet.Frontend import maxinet
import fattree

topo = fattree.FatTreeClos()
cluster = maxinet.Cluster()

exp = maxinet.Experiment(cluster, topo)
exp.setup()

topo.configRouterInterfaces(exp,topo)

#Enable OpenFlow 1.0 on all switches
for switch in exp.switches:
    exp.get_worker(switch).run_cmd(
                            'ovs-vsctl -- set Bridge %s ' % switch.name +
                            'protocols=OpenFlow10')

print exp.get_node("h1").cmd("ifconfig")  # call mininet cmd function of h1


print "waiting 5 seconds for routing algorithms on the controller to converge"
time.sleep(5)

print exp.get_node("h1").cmd("ping -c 5 10.2.4.100")

exp.stop()
