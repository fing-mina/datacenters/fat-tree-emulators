#!/usr/bin/python3


import time
import sys
sys.path.append('/home/lalberro/fat-tree-emulators-private/maxinet/UltraVioleta/clases')
from MaxiNet.Frontend import maxinet
import fattree

topo = fattree.FatTreeClos()
cluster = maxinet.Cluster()

exp = maxinet.Experiment(cluster, topo)
exp.setup()

topo.configRouterInterfaces(exp,topo)

print(exp.get_node("r30001").cmd("ifconfig"))  # call mininet cmd function of h1
#print exp.get_node("h4").cmd("ifconfig")

print ("waiting 5 seconds for routing algorithms on the controller to converge")
time.sleep(5)

#print (exp.get_node("h1").cmd("ping -c 5 10.2.4.100"))
print (exp.get_node("h1").cmd("ip route"))
print (exp.get_node("h1").cmd("ping -c 5 10.2.0.100"))
#print (exp.get_node("h4").cmd("traceroute 10.2.3.100"))
print (exp.get_node("r30001").cmd("ip route"))
#exp.CLI(locals(), globals())

exp.stop()
