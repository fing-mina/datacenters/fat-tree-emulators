from mininet.topo import Topo
from mininet.net import Mininet
from MaxiNet.Frontend import maxinet
from mininet.cli import CLI
import logging
import os
import sys
import time
import json
import subprocess

logging.basicConfig(filename='logs/fattree_bgp.log', level=logging.DEBUG)
logger = logging.getLogger(__name__)
home_dir = "/home/lalberro"

class FatTree( Topo ):
  
	CoreRouterList = []
	CoreRouterListASNs = []
	SpineRouterList = []
	SpineRouterListASNs = []
	LeafRouterList = []
	LeafRouterListASNs = []
	HostList = []

	def __init__( self, k):

		"Create Fat Tree topology"
		self.pod = k
		self.iCoreLayerRouter = int((k/2)**2)
		self.iSpineLayerRouter = int(k*k/2)
		self.iLeafLayerRouter = int(k*k/2)
		
		if hosts < 0:
			self.density = int(k/2)
		else:
			self.density = hosts
			
		self.iHost = int(self.iLeafLayerRouter * self.density)
		
		showMessage("hosts: " + str(hosts))
		showMessage("self.pod: " + str(self.pod))
		showMessage("self.iCoreLayerRouter: " + str(self.iCoreLayerRouter))
		showMessage("self.iSpineLayerRouter: " + str(self.iSpineLayerRouter))
		showMessage("self.iLeafLayerRouter: " + str(self.iLeafLayerRouter))
		showMessage("self.density: " + str(self.density))
		showMessage("self.iHost: " + str(self.iHost))
		
		self.bw_c2a = 10
		self.bw_a2e = 10
		self.bw_h2a = 10

		# Init Topo
		Topo.__init__(self)
  
		self.createTopo()
		showMessage("Finished topology creation!")

		self.createLink( bw_c2a=self.bw_c2a, 
						 bw_a2e=self.bw_a2e, 
						 bw_h2a=self.bw_h2a)
		showMessage("Finished adding links!")
		
	def createTopo(self):
		self.createCoreLayerRouter(self.iCoreLayerRouter)
		self.createSpineLayerRouter(self.iSpineLayerRouter)
		self.createLeafLayerRouter(self.iLeafLayerRouter)
		self.createHost(self.iHost)

	"""
	Create Router and Host
	"""

	def _addRouter(self, number, level, Router_list):
		for x in range(1, number+1):
			PREFIX = str(level) + "000"
			if x >= int(10):
				PREFIX = str(level) + "00"
			if x >= int(100):
				PREFIX = str(level) + "0"
			Router_list.append(self.addSwitch('r' + PREFIX + str(x), inNamespace=True))

	def createCoreLayerRouter(self, NUMBER):
		showMessage("Create Core Layer")
		self._addRouter(NUMBER, 1, self.CoreRouterList)

	def createSpineLayerRouter(self, NUMBER):
		showMessage("Create Spine Layer")
		self._addRouter(NUMBER, 2, self.SpineRouterList)

	def createLeafLayerRouter(self, NUMBER):
		showMessage("Create Leaf Layer")
		self._addRouter(NUMBER, 3, self.LeafRouterList)

	def createHost(self, NUMBER):
		showMessage("Create Host")
		for x in range(1, NUMBER+1):
			self.HostList.append(self.addHost('h' + str(x)))

	"""
	Add Link
	"""
	def createLink(self, bw_c2a=0.2, bw_a2e=0.1, bw_h2a=0.5):
		showMessage("Add link Core to Spine.")
		end = int(self.pod/2)
		for x in range(0, self.iSpineLayerRouter, end):
			for i in range(0, end):
				for j in range(0, end):
					linkopts = dict(bw=bw_c2a) 
					self.addLink(
						self.CoreRouterList[i*end+j],
						self.SpineRouterList[x+i],
						**linkopts)

		showMessage("Add link Spine to Leaf.")
		for x in range(0, self.iSpineLayerRouter, end):
			for i in range(0, end):
				for j in range(0, end):
					linkopts = dict(bw=bw_a2e) 
					self.addLink(
						self.SpineRouterList[x+i],
						self.LeafRouterList[x+j],
						**linkopts)

		showMessage("Add link Leaf to Host.")
		for x in range(0, self.iLeafLayerRouter):
			for i in range(0, self.density):
				linkopts = dict(bw=bw_h2a) 
				self.addLink(
					self.LeafRouterList[x],
					self.HostList[self.density * x + i],
					**linkopts)
	
def configRouterInterfaces(net,topo):
	end = int(topo.pod/2)
	link_configured = 0
	octet = 0
	with open('/tmp/tunnels', 'r') as filehandle:
		tunnelsList = json.load(filehandle)
	for x in range(0, topo.iSpineLayerRouter, end):
		core_interface_index = int(x / end)
		for i in range(0, end):
			spine_router = net.get_node(topo.SpineRouterList[x+i])
			spine_router.cmd("echo 1 > /proc/sys/net/ipv4/ip_forward")
			spine_zebra_file = open(f"{home_dir}/conf/zebra_{spine_router.name}.conf","a")
			spine_bgpd_file = open(f"{home_dir}/conf/bgpd_{spine_router.name}.conf","a")
			for j in range(0, end):
				if(link_configured>254):
					octet += 1
					link_configured = 0
				spine_interface_index = j
				core_router = net.get_node(topo.CoreRouterList[i*end+j])
				t_index = is_a_tunnel(tunnelsList, spine_router.name, core_router.name)
				if(t_index>=0):
					if_name_spine = if_name_core = "mn_tun"+str(t_index)
				else:
					if_name_core = f"{core_router.name}-eth{core_interface_index+1}"
					if_name_spine = f"{spine_router.name}-eth{spine_interface_index+1}"
				core_router.cmd("echo 1 > /proc/sys/net/ipv4/ip_forward")
				core_router.cmd("ifconfig %s 10.%s.%s.1/24" % (if_name_core, octet, link_configured))
				spine_router.cmd("ifconfig %s 10.%s.%s.2/24" % (if_name_spine, octet, link_configured))
				showMessage("link: "+spine_router.name+if_name_spine+" <--> "+core_router.name+if_name_core)
				spine_bgpd_file.write(" neighbor 10.%s.%s.1 remote-as %s\n" % (octet, link_configured, topo.CoreRouterListASNs[i*end+j]))
				spine_bgpd_file.write(" neighbor 10.%s.%s.1 timers 5 5\n" % (octet, link_configured))
				core_bgpd_file = open(f"{home_dir}/conf/bgpd_" + core_router.name + ".conf","a")
				core_bgpd_file.write(" neighbor 10.%s.%s.2 remote-as %s\n" % (octet, link_configured, topo.SpineRouterListASNs[x+i]))
				core_bgpd_file.write(" neighbor 10.%s.%s.2 timers 5 5\n" % (octet, link_configured))
				core_bgpd_file.close()
				core_zebra_file = open(f"{home_dir}/conf/zebra_" + core_router.name + ".conf","a")
				core_zebra_file.write("interface %s\n" % (if_name_core))
				core_zebra_file.write(" ip address 10.%s.%s.1/24\n" % (octet, link_configured))
				core_zebra_file.close()
				spine_zebra_file.write("interface %s\n" % (if_name_spine))
				spine_zebra_file.write(" ip address 10.%s.%s.2/24\n" % (octet, link_configured))
				link_configured = link_configured + 1
			spine_zebra_file.close()
			spine_bgpd_file.close()

	for x in range(0, topo.iSpineLayerRouter, end):
		for i in range(0, end):
			leaf_interface_index = i
			spine_router = net.get_node(topo.SpineRouterList[x+i])
			spine_zebra_file = open(f"{home_dir}/conf/zebra_" + spine_router.name + ".conf","a")
			spine_bgpd_file = open(f"{home_dir}/conf/bgpd_" + spine_router.name + ".conf","a")
			for j in range(0, end):
				if(link_configured>254):
					octet += 1
					link_configured = 0
				spine_interface_index = j + end
				leaf_router = net.get_node(topo.LeafRouterList[x+j])
				t_index = is_a_tunnel(tunnelsList, spine_router.name, leaf_router.name)
				if(t_index>=0):
					if_name_spine = if_name_leaf ="mn_tun"+str(t_index)
				else:
					if_name_spine = f"{spine_router.name}-eth{spine_interface_index+1}"
					if_name_leaf = f"{leaf_router.name}-eth{leaf_interface_index+1}"
				leaf_router.cmd("ifconfig %s 10.%s.%s.2/24" % (if_name_leaf, octet, link_configured))
				spine_router.cmd("ifconfig %s 10.%s.%s.1/24" % (if_name_spine, octet, link_configured))
				showMessage("link: "+spine_router.name+if_name_spine+"<-->"+leaf_router.name+if_name_leaf)
				spine_bgpd_file.write(" neighbor 10.%s.%s.2 remote-as %s\n" % (octet, link_configured, topo.LeafRouterListASNs[x+j]))
				spine_bgpd_file.write(" neighbor 10.%s.%s.2 timers 5 5\n" % (octet, link_configured))
				leaf_bgpd_file = open(f"{home_dir}/conf/bgpd_" + leaf_router.name + ".conf","a")
				leaf_bgpd_file.write(" neighbor 10.%s.%s.1 remote-as %s\n" % (octet, link_configured, topo.SpineRouterListASNs[x+i]))
				leaf_bgpd_file.write(" neighbor 10.%s.%s.1 timers 5 5\n" % (octet, link_configured))
				leaf_bgpd_file.close()
				leaf_zebra_file = open(f"{home_dir}/conf/zebra_" + leaf_router.name + ".conf","a")
				leaf_zebra_file.write("interface %s\n" % (if_name_leaf))
				leaf_zebra_file.write(" ip address 10.%s.%s.2/24\n" % (octet, link_configured))
				leaf_zebra_file.close()
				spine_zebra_file.write("interface %s\n" % (if_name_spine))
				spine_zebra_file.write(" ip address 10.%s.%s.1/24\n" % (octet, link_configured))
				link_configured = link_configured + 1
			spine_zebra_file.close()
			spine_bgpd_file.close()

	link_configured = 0
	octet = 16
	for x in range(0, topo.iLeafLayerRouter):
		router = net.get_node(topo.LeafRouterList[x])
		router.cmd("echo 1 > /proc/sys/net/ipv4/ip_forward")			
		zebra_file = open(f"{home_dir}/conf/zebra_{router.name}.conf","a")
		for i in range(0, topo.density):
			if(link_configured>254):
				octet += 1
				link_configured = 0
			host = net.get_node(topo.HostList[link_configured])
			t_index = is_a_tunnel(tunnelsList, router.name, host.name)
			if(t_index>=0):
				if_name_leaf ="mn_tun"+str(t_index)
			else:
				if_name_leaf = f"{router.name}-eth{i + topo.density+1}"
			router.cmd("ifconfig %s 172.%s.%s.1/24" % (if_name_leaf, str(octet) , str(link_configured)))
			zebra_file.write("interface %s\n"% (if_name_leaf))
			zebra_file.write(" ip address 172.%s.%s.1/24\n" % (str(octet) , str(link_configured)))
			host.cmd("ifconfig %s-eth0 172.%s.%s.100/24" % (host.name, str(octet), str(link_configured)))
			host.cmd("ip route add default via 172.%s.%s.1" % (str(octet), str(link_configured)))
			link_configured = link_configured + 1
		
		zebra_file.close()

def is_a_tunnel(tunnelsList, node1, node2):
	index = -1
	for x in tunnelsList:
		if(x['node1'] == node1 and x['node2'] == node2) or (x['node1'] == node2 and x['node2'] == node1):
			index = tunnelsList.index(x)
	return index
		
def startDaemons(net,topo):
	for x in range(0, topo.iCoreLayerRouter):
		router = net.get_node(topo.CoreRouterList[x])
		router.cmd(f"/usr/lib/frr/zebra -f {home_dir}/conf/zebra_%s.conf -d -i /tmp/zebra_%s.pid > {home_dir}/logs/%s_zebra.stdout 2>&1" % (router.name, router.name, router.name))
		router.waitOutput()
		router.cmd(f"/usr/lib/frr/bgpd --ecmp 64 -f {home_dir}/conf/bgpd_%s.conf -d -i /tmp/bgpd_%s.pid --log file:{home_dir}/logs/%s_bgpd.stdout" % (router.name, router.name, router.name), shell=True)
		router.waitOutput()
	
	showMessage("End Cores")

	for x in range(0, topo.iSpineLayerRouter):
		router = net.get_node(topo.SpineRouterList[x])
		router.cmd(f"/usr/lib/frr/zebra -f {home_dir}/conf/zebra_%s.conf -d -i /tmp/zebra_%s.pid > {home_dir}/logs/%s_zebra.stdout 2>&1" % (router.name, router.name, router.name))
		router.waitOutput()
		router.cmd(f"/usr/lib/frr/bgpd --ecmp 64 -f {home_dir}/conf/bgpd_%s.conf -d -i /tmp/bgpd_%s.pid --log file:{home_dir}/logs/%s_bgpd.stdout" % (router.name, router.name, router.name), shell=True)
		router.waitOutput()

	showMessage("End Spines")

	for x in range(0, topo.iLeafLayerRouter):
		router = net.get_node(topo.LeafRouterList[x])
		router.cmd(f"/usr/lib/frr/zebra -f {home_dir}/conf/zebra_%s.conf -d -i /tmp/zebra_%s.pid > {home_dir}/logs/%s_zebra.stdout 2>&1" % (router.name, router.name, router.name))
		router.waitOutput()
		router.cmd(f"/usr/lib/frr/bgpd --ecmp 64 -f {home_dir}/conf/bgpd_%s.conf -d -i /tmp/bgpd_%s.pid --log file:{home_dir}/logs/%s_bgpd.stdout" % (router.name, router.name, router.name), shell=True)
		router.waitOutput()

	showMessage("End Leaves")

def createBGPZebraHeaderFiles(net,topo):
	end = int(topo.pod/2)
	asn_nr = 65534
	for x in range(0, topo.iCoreLayerRouter):
		topo.CoreRouterListASNs.append(str(asn_nr))
		router = net.get_node(topo.CoreRouterList[x])
		file = open(f"{home_dir}/conf/bgpd_" + router.name + ".conf","w")
		file.write("hostname " + router.name + "\n")
		file.write("password en\n")
		file.write("enable password en\n")
		file.write("log file /tmp/" + router.name + "-bgpd.log\n")
		file.write("router bgp " + str(asn_nr) + "\n")
		file.write(" bgp router-id 10.0.254.1\n")
		file.write(" bgp bestpath as-path multipath-relax\n")
		file.write(" no bgp ebgp-requires-policy\n")
		file.close()
		file = open(f"{home_dir}/conf/zebra_" + router.name + ".conf","w")
		file.write("hostname " + router.name + "\n")
		file.write("password en\n")
		file.write("enable password en\n")
		file.write("log file /tmp/" + router.name + "-zebra.log\n")
		file.close()

	for x in range(0, topo.iSpineLayerRouter):
		pod_nr = int(x / end)
		asn_nr = 65000 + (pod_nr * topo.pod)
		topo.SpineRouterListASNs.append(str(asn_nr))
		router = net.get_node(topo.SpineRouterList[x])
		file = open(f"{home_dir}/conf/bgpd_" + router.name + ".conf","w")
		file.write("hostname " + router.name + "\n")
		file.write("password en\n")
		file.write("enable password en\n")
		file.write("log file /tmp/" + router.name + "-bgpd.log\n")
		file.write("router bgp " + str(asn_nr) + "\n")
		file.write(" bgp router-id 10.0.254.2\n")
		file.write(" bgp bestpath as-path multipath-relax\n")
		file.write(" no bgp ebgp-requires-policy\n")
		file.close()
		file = open(f"{home_dir}/conf/zebra_" + router.name + ".conf","w")
		file.write("hostname " + router.name + "\n")
		file.write("password en\n")
		file.write("enable password en\n")
		file.write("log file /tmp/" + router.name + "-zebra.log\n")
		file.close()

	for x in range(0, topo.iLeafLayerRouter, end):
		pod_nr = int(x / end)
		for i in range(0, end):
			asn_nr = 65000 + (pod_nr * topo.pod) + i + 1
			topo.LeafRouterListASNs.append(str(asn_nr))
			router = net.get_node(topo.LeafRouterList[x + i])
			file = open(f"{home_dir}/conf/bgpd_" + router.name + ".conf","w")
			file.write("hostname " + router.name + "\n")
			file.write("password en\n")
			file.write("enable password en\n")
			file.write("log file /tmp/" + router.name + "-bgpd.log\n")
			file.write("router bgp " + str(asn_nr) + "\n")
			file.write(" bgp router-id 10.1.254.2\n")
			file.write(" bgp bestpath as-path multipath-relax\n")
			file.write(" no bgp ebgp-requires-policy\n")
			file.close()
			file = open(f"{home_dir}/conf/zebra_" + router.name + ".conf","w")
			file.write("hostname " + router.name + "\n")
			file.write("password en\n")
			file.write("enable password en\n")
			file.write("log file /tmp/" + router.name + "-zebra.log\n")
			file.close()

def createBGPZebraFooterFiles(net,topo):
	for x in range(0, topo.iCoreLayerRouter):
		router = net.get_node(topo.CoreRouterList[x])
		file = open(f"{home_dir}/conf/bgpd_" + router.name + ".conf","a")
		file.write(" redistribute connected\n")
		file.write("log stdout\n")
		file.close()

	for x in range(0, topo.iSpineLayerRouter):
		router = net.get_node(topo.SpineRouterList[x])
		file = open(f"{home_dir}/conf/bgpd_" + router.name + ".conf","a")
		file.write(" redistribute connected\n")
		file.write("log stdout\n")
		file.close()

	for x in range(0, topo.iLeafLayerRouter):
		router = net.get_node(topo.LeafRouterList[x])
		file = open(f"{home_dir}/conf/bgpd_" + router.name + ".conf","a")
		file.write(" redistribute connected\n")
		file.write("log stdout\n")
		file.close()

def showMessage(message):
	logger.debug(message)
	sys.stdout.write(message + "\n")
	sys.stdout.flush

def stopFlag(net,topo):
	showMessage("TODO")

def preCalculatedTables(net,topo):
	showMessage("TODO")

def dumpTables(net,topo):
	showMessage("Dumping forwarding tables")
	for x in range(0, topo.iCoreLayerRouter):
		router = net.get_node(topo.CoreRouterList[x])
		output = router.cmd("ip route")
		file = open(f"{home_dir}/outputs/table_" + router.name + ".txt","w")
		file.write(output)
		file.close()

	for x in range(0, topo.iSpineLayerRouter):
		router = net.get_node(topo.SpineRouterList[x])
		output = router.cmd("ip route")
		file = open(f"{home_dir}/outputs/table_" + router.name + ".txt","w")
		file.write(output)
		file.close()

	for x in range(0, topo.iLeafLayerRouter):
		router = net.get_node(topo.LeafRouterList[x])
		output = router.cmd("ip route")
		file = open(f"{home_dir}/outputs/table_" + router.name + ".txt","w")
		file.write(output)
		file.close()

def distributeConfigFiles():
	#ssh public key of the "master" must be in place on each worker
	#TODO: read IPs and user from Maxinet conf file
	workers = ['10.0.1.201']
	for ip in workers:
		os.system(f"scp {home_dir}/conf/* lalberro@{ip}:~/conf/")
	

def main(k):
	os.system("rm -f /tmp/*.pid logs/*.stdout")
	os.system("killall -9 bgpd zebra > /dev/null 2>&1")
	
	if not os.path.exists(f"{home_dir}/conf"):
		os.makedirs(f'{home_dir}/conf')
	else:
		os.system(f"rm -f {home_dir}/conf/*")
	
	if not os.path.exists(f'logs'):
		os.makedirs(f'logs')

	if not os.path.exists(f'{home_dir}/outputs'):
		os.makedirs(f'{home_dir}/outputs')
	else:
		os.system(f"rm -f {home_dir}/outputs/*")
	
	if not os.path.exists(f'{home_dir}/pcaps'):
		os.makedirs(f'{home_dir}/pcaps')
	else:
		os.system(f"rm -f {home_dir}/pcaps/*")
	
	topo=FatTree(k)
	showMessage("topo")
	cluster = maxinet.Cluster()
	showMessage("cluster")
	exp = maxinet.Experiment(cluster, topo)
	showMessage("exp")
	exp.setup()
	showMessage("setup")

	time.sleep(10)
	
	createBGPZebraHeaderFiles(exp,topo)
	showMessage("BGP and Zebra config Files created!")
	
	configRouterInterfaces(exp,topo)
	showMessage("Router interfaces configured!")
	
	createBGPZebraFooterFiles(exp,topo)
	showMessage("BGP and Zebra config footer added!")

	distributeConfigFiles()
	showMessage("Distributing BGP and Zebra config files")
	
	startDaemons(exp,topo)
	showMessage("Deamons started!")
	
	showMessage("*** Starting network")
	
	#stopFlag(net, topo)
	#preCalculatedTables(net,topo)
	time.sleep(5)
	#dumpTables(exp, topo)
	#print(exp.get_node("h1").cmd("ifconfig")) 
	#print(exp.get_node("h6").cmd("ifconfig")) 
	#print (exp.get_node("r30001").cmd("ifconfig"))
	#print(exp.get_node("h0004").cmd("ifconfig"))
	#print (exp.get_node("h1").cmd("ping -c 5 10.2.4.100"))
	#print (exp.get_node("h1").cmd("ip route"))
	#print (exp.get_node("h1").cmd("ping -c 5 172.16.0.1"))
	#print (exp.get_node("h1").cmd("traceroute 172.16.5.100"))
	# print (exp.get_node("r30001").cmd("ifconfig"))
	print (exp.get_node("r30001").cmd("ip route"))
	#print (exp.get_node("h4").cmd("traceroute 10.2.3.100"))
	# print(exp.get_node("h1").cmd("ping -c 5 172.16.3.100")) 
	# print(exp.get_node("h1").cmd("ping -c 5 172.16.4.100"))
	# print(exp.get_node("h1").cmd("ping -c 5 172.16.5.100"))
	# print(exp.get_node("h1").cmd("ping -c 5 172.16.6.100"))
	#print(exp.get_node("r30001").cmd("ping -c 5 10.0.16.1")) 
	exp.CLI(locals(), globals())
	#print(exp.get_node("r30001").cmd("ps aux | grep frr"))
	 
	exp.stop()

	showMessage("*** Stopping network")
	os.system("killall -9 bgpd zebra > /dev/null 2>&1")

if __name__ == "__main__":
	if len(sys.argv) < 2:
		print ('fattree_isis.py k <hosts per leaf>')
		sys.exit(2)
	
	k = int(sys.argv[1])
	hosts = -1
	
	if k & 1:
		print ('k must be even')
		sys.exit(2)

	if len(sys.argv) > 2:
		hosts = int(sys.argv[2])

	if hosts > k/2:
		print ('hosts must be <= k/2')
		sys.exit(2)

	main(k)
